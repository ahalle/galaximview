import unittest

import hypothesis.strategies as st
import numpy.testing
from hypothesis import given
from hypothesis.extra.numpy import arrays

from galaximview.readsnapshot import *


class Test(unittest.TestCase):
    @given(arrays(np.float, (10, 3), elements=st.floats(max_value=1e10, min_value=-1e10)),
           arrays(np.float, (10,), elements=st.floats(max_value=1e6, min_value=0)))
    def test_calc_com(self, pos, mass):
        if not np.any(mass):
            with self.assertRaises(ValueError):
                com = calc_com(pos, mass)
        else:
            com = calc_com(pos, mass)
            pos -= com
            com = calc_com(pos, mass)
            numpy.testing.assert_array_almost_equal(com, np.zeros_like(com), decimal=5)
