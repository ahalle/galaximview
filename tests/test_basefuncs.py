import unittest

import hypothesis.strategies as st
import numpy.testing
from hypothesis import given, example
from hypothesis.extra.numpy import arrays, array_shapes

from galaximview.units import *


class TestBaseTools(unittest.TestCase):

    def setUp(self):
        self.ex3d = np.array([1, 0, 0])
        self.ey3d = np.array([0, 1, 0])
        self.ez3d = np.array([0, 0, 1])
        self.base3d = np.vstack((self.ex3d, self.ey3d, self.ez3d))
        self.ex2d = np.array([1, 0])
        self.ey2d = np.array([0, 1])
        self.base2d = np.vstack((self.ex2d, self.ey2d))
        self.random1dvect = np.random.rand(10) * 2 - 1.
        self.randomposvect = np.random.rand(10, 3) * 2 - 1.

    @given(arrays(np.int, (100,), elements=st.integers(min_value=0, max_value=2 ** 32 - 1)),
           st.floats(min_value=0, max_value=1))
    @example(np.arange(100), 1)
    # @settings(verbosity=Verbosity.verbose)
    def test_subset_of_indexes(self, indexes, fraction):
        subset = BaseTools.subset_of_indexes(indexes, fraction)
        self.assertTrue(np.all(np.in1d(subset, indexes)))
        self.assertTrue(len(np.shape(subset)) == 1)
        sizeofsubset = np.size(subset)
        sizeofinds = np.size(indexes)
        theor_sizeofsubset = int(fraction * sizeofinds)
        self.assertTrue(sizeofsubset == theor_sizeofsubset)

    @given(arrays(np.float, array_shapes()) | arrays(np.int, array_shapes()))
    # @settings(verbosity=Verbosity.verbose)
    def test_normalise_vector_to_unity(self, arr):

        if ((not np.any(arr)) | (np.any(np.isnan(arr)))):
            with self.assertRaises(ValueError):
                res = BaseTools.normalise_vector_to_unity(arr)
        else:
            norm = la.norm(arr)
            if ((norm == np.inf) | (norm < np.finfo(float).eps)):
                with self.assertRaises(ValueError):
                    res = BaseTools.normalise_vector_to_unity(arr)
            else:
                res = BaseTools.normalise_vector_to_unity(arr)
                normres = 0
                ravres = np.ravel(res)
                for i in range(0, np.size(arr)):
                    normres += ravres[i] ** 2
                normres = np.sqrt(normres)
                self.assertAlmostEqual(normres, 1)

    @classmethod
    def generate_random_ortho_normed_direct_2d_basis(cls, seed=None):
        """
        Can be given a seed for identical reproduction.
        """
        if (seed is not None):
            np.random.seed(seed)
        ex = np.random.rand(2)
        ex = BaseTools.normalise_vector_to_unity(ex)
        ey = np.zeros_like(ex)
        ey[0] = -ex[1]
        ey[1] = ex[0]
        return np.vstack((ex, ey))

    @classmethod
    def generate_random_ortho_normed_direct_3d_basis(cls, seed=None):
        """
        Can be given a seed for identical reproduction.
        """
        if (seed is not None):
            np.random.seed(seed)
        ex = np.random.rand(3)
        ex = BaseTools.normalise_vector_to_unity(ex)
        buff = np.random.rand(3)
        buff = BaseTools.normalise_vector_to_unity(buff)
        diff = abs(buff - ex)
        maxiter = 10
        i = 0
        while (np.all(np.round(diff, 7) == 0)) & (i < maxiter):
            buff = np.random.rand(3)
            buff = BaseTools.normalise_vector_to_unity(buff)
            diff = abs(buff - ex)
            i += 1
        ey = np.cross(ex, buff)
        ey = BaseTools.normalise_vector_to_unity(ey)
        ez = np.cross(ex, ey)
        return np.vstack((ex, ey, ez))

    def test_generate_random_ortho_normed_direct_3d_basis(self):
        basis = self.generate_random_ortho_normed_direct_3d_basis()
        self.assertEqual(BaseTools.check_ortho_normed_direct_basis(basis), True)

    def test_project_on_base(self):
        # checks that base vectors (and of type int) are obtained back
        for vect in (self.ex3d, self.ey3d, self.ez3d):
            res = BaseTools.project_on_orthonormed_direct_basis(vect, self.base3d)
            self.assertTrue(np.all(res == vect))
        for vect in (self.ex2d, self.ey2d):
            res = BaseTools.project_on_orthonormed_direct_basis(vect, self.base2d)
            self.assertTrue(np.all(res == vect))

        # checks that random position array written on cartesian base is unchanged when re-projected on the base
        res = BaseTools.project_on_orthonormed_direct_basis(self.randomposvect, self.base3d)
        self.assertTrue(np.all(res == self.randomposvect))

        # check that projection of random position like array on random orhtonormed direct base gives back original
        # array when projected on inverse base
        basis = self.generate_random_ortho_normed_direct_3d_basis()
        invbasis = BaseTools.invert_basis(basis)
        res = BaseTools.project_on_orthonormed_direct_basis(self.randomposvect, basis)
        resinv = BaseTools.project_on_orthonormed_direct_basis(res, invbasis)
        numpy.testing.assert_array_almost_equal(resinv, self.randomposvect)

    def test_rotate_around_vector(self):
        alpharot = np.random.rand() * 2 * np.pi
        # Tests that rotation around ex, ey or ez gives good result.
        for k in range(0, 3):
            res = BaseTools.rotate_around_vector(self.randomposvect, self.base3d[k], alpharot)
            expectedres = np.zeros_like(res)
            expectedres[:, k - 2] = self.randomposvect[:, k - 2] * np.cos(alpharot) - self.randomposvect[:, k - 1] \
                * np.sin(alpharot)
            expectedres[:, k - 1] = self.randomposvect[:, k - 1] * np.cos(alpharot) \
                + self.randomposvect[:, k - 2] * np.sin(alpharot)
            expectedres[:, k] = self.randomposvect[:, k]
            numpy.testing.assert_array_almost_equal(res, expectedres)

        elos = np.random.rand(3)
        elos = BaseTools.normalise_vector_to_unity(elos)
        diffex = abs(elos - self.ex3d)
        diffey = abs(elos - self.ey3d)
        diffez = abs(elos - self.ez3d)
        maxiter = 10
        i = 0
        while ((np.all(np.round(diffex, 3) == 0)) | (np.all(np.round(diffey, 3) == 0)) | (
                np.all(np.round(diffez, 3) == 0))) & (i < maxiter):
            elos = np.random.rand(3)
            elos = BaseTools.normalise_vector_to_unity(elos)
            diffex = abs(elos - self.ex3d)
            diffey = abs(elos - self.ey3d)
            diffez = abs(elos - self.ez3d)
            i += 1

        # Tests that rotation of 0 around random vector gives back original array
        res = BaseTools.rotate_around_vector(self.randomposvect, elos, 0)
        numpy.testing.assert_array_almost_equal(res, self.randomposvect)
        # Tests that rotation of random angle alpharot around random vector followed by rotation of 2*pi-alpharot
        # gives back original array
        res = BaseTools.rotate_around_vector(self.randomposvect, elos, alpharot)
        resend = BaseTools.rotate_around_vector(res, elos, 2 * np.pi - alpharot)
        numpy.testing.assert_array_almost_equal(resend, self.randomposvect)


class Test(unittest.TestCase):
    def setUp(self):
        pass

    @given(st.floats(allow_nan=False) | st.integers() | st.booleans() | st.text() | arrays(np.float, array_shapes()))
    # @settings(verbosity=Verbosity.verbose)
    def test_identity_function(self, x):
        y = identity_function(x)
        if isinstance(x, np.ndarray):
            numpy.testing.assert_array_equal(y, x)
        else:
            self.assertEqual(x, y)

    @given(st.floats(min_value=0, max_value=1e10, exclude_min=True, allow_nan=False) |
           arrays(np.float, array_shapes(),
                  elements=st.floats(min_value=0, exclude_min=True, max_value=1e10, allow_nan=False)))
    def test_inverse_function(self, x):
        for function in [identity_function, np.log10, np.log, np.sqrt]:
            y = function(x)
            z = inverse_function(function, y)
            # print(x, y, z, function)
        if isinstance(x, np.ndarray):
            numpy.testing.assert_array_almost_equal(np.float32(z), np.float32(x))
        else:
            self.assertAlmostEqual(np.float32(z), np.float32(x))

    def test_dic_unity_gravity_const_units(self):
        dicunits = dic_unity_gravity_const_units()
        print(dicunits['gravity_constant_internal'])
        self.assertAlmostEqual(dicunits['gravity_constant_internal'], 1.)
