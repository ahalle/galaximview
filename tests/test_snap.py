from unittest import TestCase

import numpy as np
import numpy.testing
import itertools
from galaximview.snap import *


class TestSnap(TestCase):
    def setUp(self):
        fdirin = '/data/G4examples/G2-galaxy/formatG1G2'
        fnamein = 'snapshot_006'
        self.snap = Snap(fdirin, fnamein, gadget2_header=True, ntypes=3)

    def test_get_indexes_from_ids(self):
        inds = np.arange(100, 200)
        ids = self.snap.get_ids_from_indexes(inds)
        indsback = self.snap.get_indexes_from_ids(ids)
        numpy.testing.assert_array_equal(indsback, inds)

    def test_surface_density_of_radius(self):
        rmin = 2.
        rmax = 20.
        listcomps = self.snap.listcomps
        listradconvtype = [None, 'astro']
        listhistconvtype = [None, 'astro', 'cgs']
        for (compstr, rad_conv_type, hist_conv_type) in itertools.product(listcomps, listradconvtype,
                                                                          listhistconvtype):
            x, h = self.snap.surface_density_of_radius(compstr, xrange=(rmin, rmax), rad_conv_type=rad_conv_type,
                                                       surfdens_conv_type=hist_conv_type)
            dx = x[1] - x[0]
            edges_ini = np.array([x[0] - 0.5 * dx])
            edges_fin = np.array([x[-1] + 0.5 * dx])
            edges = np.concatenate((edges_ini, 0.5 * (x[1:] + x[:-1]), edges_fin))
            totmass_in_hist = np.pi * sum(h * (edges[1:] ** 2 - edges[:-1] ** 2))
            totmass_in_hist *= BaseUnitsDefs.get_conversion_factor_from_string('mass', self.snap.dicunits,
                                                                               conv_type=hist_conv_type) \
                               / BaseUnitsDefs.get_conversion_factor_from_string('surfdens', self.snap.dicunits,
                                                                                 conv_type=hist_conv_type)
            indscomp = self.snap.get_indexes_of_component(compstr)
            radcyl = SpatialCoordsFromCart(self.snap.data['pos']).cylindrical_radius()
            indsinradsrange = np.where((radcyl >= rmin) & (radcyl <= rmax))
            indsofcompinradsrange = np.intersect1d(indscomp, indsinradsrange)
            theor_mass = sum(self.snap.data['mass'][indsofcompinradsrange]) \
                         * BaseUnitsDefs.get_conversion_factor_from_string('mass', self.snap.dicunits,
                                                                           conv_type=hist_conv_type)
            self.assertAlmostEqual(np.float32(totmass_in_hist), np.float32(theor_mass))

    def test_volume_density_of_radius(self):
        rmin = 2.
        rmax = 20.
        listcomps = self.snap.listcomps
        listradconvtype = [None, 'astro']
        listhistconvtype = [None, 'astro', 'cgs']
        for (compstr, rad_conv_type, hist_conv_type) in itertools.product(listcomps, listradconvtype,
                                                                          listhistconvtype):
            x, h = self.snap.volume_density_of_radius(compstr, xrange=(rmin, rmax), rad_conv_type=rad_conv_type,
                                                      voldens_conv_type=hist_conv_type)
            dx = x[1] - x[0]
            edges_ini = np.array([x[0] - 0.5 * dx])
            edges_fin = np.array([x[-1] + 0.5 * dx])
            edges = np.concatenate((edges_ini, 0.5 * (x[1:] + x[:-1]), edges_fin))
            totmass_in_hist = 4. / 3. * np.pi * sum(h * (edges[1:] ** 3 - edges[:-1] ** 3))
            totmass_in_hist *= BaseUnitsDefs.get_conversion_factor_from_string('mass', self.snap.dicunits,
                                                                               conv_type=hist_conv_type) \
                               / BaseUnitsDefs.get_conversion_factor_from_string('voldens', self.snap.dicunits,
                                                                                 conv_type=hist_conv_type)
            indscomp = self.snap.get_indexes_of_component(compstr)
            radsph = SpatialCoordsFromCart(self.snap.data['pos']).spherical_radius()
            indsinradsrange = np.where((radsph >= rmin) & (radsph <= rmax))
            indsofcompinradsrange = np.intersect1d(indscomp, indsinradsrange)
            theor_mass = sum(self.snap.data['mass'][indsofcompinradsrange])
            theor_mass *= BaseUnitsDefs.get_conversion_factor_from_string('mass', self.snap.dicunits, conv_type=hist_conv_type)
            self.assertAlmostEqual(np.float32(totmass_in_hist), np.float32(theor_mass))

    def test_mass_in_radius(self):
        rmin = 2.
        rmax = 20.
        listcomps = self.snap.listcomps
        listradconvtype = [None, 'astro']
        listhistconvtype = [None, 'astro', 'cgs']
        for (compstr, rad_conv_type, hist_conv_type) in itertools.product(listcomps, listradconvtype,
                                                                          listhistconvtype):
            x, h = self.snap.mass_in_radius(compstr, radstr='radcyl', xrange=(rmin, rmax), rad_conv_type=rad_conv_type,
                                            mass_conv_type=hist_conv_type)
            totmass_in_hist = h[-1]
            indscomp = self.snap.get_indexes_of_component(compstr)
            radcyl = SpatialCoordsFromCart(self.snap.data['pos']).cylindrical_radius()
            indsinradsrange = np.where((radcyl <= rmax))
            indsofcompinradsrange = np.intersect1d(indscomp, indsinradsrange)
            theor_mass = sum(self.snap.data['mass'][indsofcompinradsrange])
            theor_mass *= BaseUnitsDefs.get_conversion_factor_from_string('mass', self.snap.dicunits, conv_type=hist_conv_type)
            self.assertAlmostEqual(np.float32(totmass_in_hist), np.float32(theor_mass))

            x, h = self.snap.mass_in_radius(compstr, radstr='radspher', xrange=(rmin, rmax), rad_conv_type=rad_conv_type,
                                            mass_conv_type=hist_conv_type)
            totmass_in_hist = h[-1]
            indscomp = self.snap.get_indexes_of_component(compstr)
            radspher = SpatialCoordsFromCart(self.snap.data['pos']).spherical_radius()
            indsinradsrange = np.where((radspher <= rmax))
            indsofcompinradsrange = np.intersect1d(indscomp, indsinradsrange)
            theor_mass = sum(self.snap.data['mass'][indsofcompinradsrange])
            theor_mass *= BaseUnitsDefs.get_conversion_factor_from_string('mass', self.snap.dicunits, conv_type=hist_conv_type)
            self.assertAlmostEqual(np.float32(totmass_in_hist), np.float32(theor_mass))
