import unittest

import hypothesis.strategies as st
import numpy.testing
from hypothesis import given
from hypothesis.extra.numpy import arrays

from galaximview.coords import *


class TestCoordsBase(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomphi = np.random.rand(self.nparts) * 2 * np.pi - np.pi
        self.randomtheta = np.random.rand(self.nparts) * np.pi

    def test_cylindrical_and_cart_bases(self):
        ercyl, ephi, ez = Coords.cylindrical_basis_vects_in_cart_basis(self.randomphi)
        basealt = np.hstack((ercyl.transpose(), ephi.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        ex, ey, ez = Coords.cartesian_basis_vects_in_cyl_basis(self.randomphi)
        basealt = np.hstack((ex.transpose(), ey.transpose(), ez.transpose()))
        indivbasesinv = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            baseinv = BaseTools.invert_basis(indivbases[i])
            numpy.testing.assert_array_almost_equal(baseinv, indivbasesinv[i])

    def test_cylindrical_and_spher_bases(self):
        ercyl, ephi, ez = Coords.cylindrical_basis_vects_in_spher_basis(self.randomtheta)
        basealt = np.hstack((ercyl.transpose(), ephi.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        erspher, ethetaspher, ephispher = Coords.spherical_basis_vects_in_cyl_basis(self.randomtheta)
        basealt = np.hstack((erspher.transpose(), ethetaspher.transpose(), ephispher.transpose()))
        indivbasesinv = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            baseinv = BaseTools.invert_basis(indivbases[i])
            numpy.testing.assert_array_almost_equal(baseinv, indivbasesinv[i])

    def test_spher_and_cart_bases(self):
        erspher, ethetaspher, ephispher = Coords.spherical_basis_vects_in_cart_basis(self.randomtheta, self.randomphi)
        basealt = np.hstack((erspher.transpose(), ethetaspher.transpose(), ephispher.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        ex, ey, ez = Coords.cartesian_basis_vects_in_spher_basis(self.randomtheta, self.randomphi)
        basealt = np.hstack((ex.transpose(), ey.transpose(), ez.transpose()))
        indivbasesinv = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            baseinv = BaseTools.invert_basis(indivbases[i])
            numpy.testing.assert_array_almost_equal(baseinv, indivbasesinv[i])


class TestSpatialCoordsFromCart(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomposvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.sp = SpatialCoordsFromCart(self.randomposvect)

    @given(arrays(np.float, (10, 3), elements=st.floats(allow_nan=False, allow_infinity=False)))
    # @settings(verbosity=Verbosity.verbose)
    def test_cylindrical_radius(self, arr):
        sphyp = SpatialCoordsFromCart(arr)
        rad = sphyp.cylindrical_radius()
        # numpy.testing.assert_array_almost_equal(rad ** 2, arr[:, 0] ** 2 + arr[:, 1] ** 2)
        numpy.testing.assert_allclose(rad ** 2, arr[:, 0] ** 2 + arr[:, 1] ** 2)

    def test_cylindrical_radius_myself(self):
        rad = self.sp.cylindrical_radius()
        numpy.testing.assert_array_almost_equal(rad ** 2, self.randomposvect[:, 0] ** 2 + self.randomposvect[:, 1] ** 2)

    def test_spherical_radius(self):
        rad = self.sp.spherical_radius()
        numpy.testing.assert_array_almost_equal(rad ** 2,
                                                self.randomposvect[:, 0] ** 2 + self.randomposvect[:, 1] ** 2 +
                                                self.randomposvect[:, 2] ** 2)

    def test_spherical_theta(self):
        theta = self.sp.spherical_theta()
        rad = self.sp.spherical_radius()
        self.assertTrue(np.all(theta >= 0))
        self.assertTrue(np.all(theta < np.pi))
        numpy.testing.assert_array_almost_equal(rad * np.cos(theta), self.randomposvect[:, 2])

    def test_phi(self):
        phi = self.sp.phi()
        rad = self.sp.cylindrical_radius()
        self.assertTrue(np.all(phi >= -np.pi))
        self.assertTrue(np.all(phi < np.pi))
        numpy.testing.assert_array_almost_equal(rad * np.cos(phi), self.randomposvect[:, 0])
        numpy.testing.assert_array_almost_equal(rad * np.sin(phi), self.randomposvect[:, 1])

    def test_tocyl(self):
        cylpos = self.sp.tocyl_pos()
        numpy.testing.assert_array_almost_equal(cylpos[:, 0], self.sp.cylindrical_radius())
        numpy.testing.assert_array_almost_equal(cylpos[:, 1], self.sp.phi())
        numpy.testing.assert_array_almost_equal(cylpos[:, 2], self.randomposvect[:, 2])
        spcyl = SpatialCoordsFromCyl(cylpos)
        cartpos = spcyl.tocart_pos()
        numpy.testing.assert_array_almost_equal(cartpos, self.randomposvect)

    def test_tocyl_sp_object(self):
        spcyl = self.sp.tocyl_sp_object()
        spback = SpatialCoordsFromCyl(spcyl.cylpos).tocart_sp_object()
        numpy.testing.assert_array_almost_equal(self.sp.pos, spback.pos)

    def test_tospher(self):
        spherpos = self.sp.tospher_pos()
        numpy.testing.assert_array_almost_equal(spherpos[:, 0], self.sp.spherical_radius())
        numpy.testing.assert_array_almost_equal(spherpos[:, 1], self.sp.spherical_theta())
        numpy.testing.assert_array_almost_equal(spherpos[:, 2], self.sp.phi())
        spspher = SpatialCoordsFromSpher(spherpos)
        cartpos = spspher.tocart_pos()
        numpy.testing.assert_array_almost_equal(cartpos, self.randomposvect)

    def test_tospher_sp_object(self):
        spspher = self.sp.tospher_sp_object()
        spback = SpatialCoordsFromSpher(spspher.spherpos).tocart_sp_object()
        numpy.testing.assert_array_almost_equal(self.sp.pos, spback.pos)

    def test_spherical_base(self):
        erspher, ethetaspher, ephi = self.sp.spherical_base_vects_in_cart_base()
        basealt = np.hstack((erspher.transpose(), ethetaspher.transpose(), ephi.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            posproj = BaseTools.project_on_orthonormed_direct_basis(self.randomposvect[i], indivbases[i])
            numpy.testing.assert_array_almost_equal(posproj[0], self.sp.spherical_radius()[i])
            numpy.testing.assert_array_almost_equal(posproj[1], 0)
            numpy.testing.assert_array_almost_equal(posproj[2], 0)
            baseinv = BaseTools.invert_basis(indivbases[i])
            posprojback = BaseTools.project_on_orthonormed_direct_basis(posproj, baseinv)
            numpy.testing.assert_array_almost_equal(posprojback, self.randomposvect[i])

    def test_cylindrical_base(self):
        ercyl, ephi, ez = self.sp.cylindrical_base_vects_in_cart_base()
        basealt = np.hstack((ercyl.transpose(), ephi.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            posproj = BaseTools.project_on_orthonormed_direct_basis(self.randomposvect[i], indivbases[i])
            numpy.testing.assert_array_almost_equal(posproj[0], self.sp.cylindrical_radius()[i])
            numpy.testing.assert_array_almost_equal(posproj[1], 0)
            numpy.testing.assert_array_almost_equal(posproj[2], self.randomposvect[i][2])
            baseinv = BaseTools.invert_basis(indivbases[i])
            posprojback = BaseTools.project_on_orthonormed_direct_basis(posproj, baseinv)
            numpy.testing.assert_array_almost_equal(posprojback, self.randomposvect[i])


class TestSpatialCoordsFromCyl(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomcylposvect = np.zeros((self.nparts, 3))
        self.randomcylposvect[:, 0] = np.random.rand(self.nparts)
        self.randomcylposvect[:, 1] = np.random.rand(self.nparts) * 2 * np.pi - np.pi
        self.randomcylposvect[:, 2] = np.random.rand(self.nparts) * 2 - 1.
        self.randomcylposvectincylbase = np.copy(self.randomcylposvect)
        self.randomcylposvectincylbase[:, 1] = 0
        self.spcyl = SpatialCoordsFromCyl(self.randomcylposvect)

    def test_spherical_radius(self):
        rads = self.spcyl.spherical_radius()
        numpy.testing.assert_array_almost_equal(rads ** 2,
                                                self.randomcylposvect[:, 0] ** 2 + self.randomcylposvect[:, 2] ** 2)
        xcart = self.spcyl.xcart()
        ycart = self.spcyl.ycart()
        zcart = self.spcyl.zcart()
        numpy.testing.assert_array_almost_equal(rads ** 2, xcart ** 2 + ycart ** 2 + zcart ** 2)

    def test_spherical_theta(self):
        theta = self.spcyl.spherical_theta()
        rads = self.spcyl.spherical_radius()
        numpy.testing.assert_array_almost_equal(rads * np.cos(theta), self.randomcylposvect[:, 2])

    def test_phi(self):
        phi = self.spcyl.phi()
        numpy.testing.assert_array_almost_equal(phi, self.randomcylposvect[:, 1])

    def test_tocart(self):
        cartpos = self.spcyl.tocart_pos()
        numpy.testing.assert_array_almost_equal(cartpos[:, 0], self.spcyl.xcart())
        numpy.testing.assert_array_almost_equal(cartpos[:, 1], self.spcyl.ycart())
        numpy.testing.assert_array_almost_equal(cartpos[:, 2], self.spcyl.zcart())
        spcart = SpatialCoordsFromCart(cartpos)
        cylpos = spcart.tocyl_pos()
        numpy.testing.assert_array_almost_equal(cylpos, self.randomcylposvect)

    def test_tocart_sp_object(self):
        spcart = self.spcyl.tocart_sp_object()
        spback = SpatialCoordsFromCart(spcart.pos).tocyl_sp_object()
        numpy.testing.assert_array_almost_equal(self.spcyl.cylpos, spback.cylpos)

    def test_tospher(self):
        spherpos = self.spcyl.tospher_pos()
        numpy.testing.assert_array_almost_equal(spherpos[:, 0], self.spcyl.spherical_radius())
        numpy.testing.assert_array_almost_equal(spherpos[:, 1], self.spcyl.spherical_theta())
        numpy.testing.assert_array_almost_equal(spherpos[:, 2], self.spcyl.phi())
        spspher = SpatialCoordsFromSpher(spherpos)
        cylpos = spspher.tocyl_pos()
        numpy.testing.assert_array_almost_equal(cylpos, self.randomcylposvect)

    def test_tospher_sp_object(self):
        spspher = self.spcyl.tospher_sp_object()
        spback = SpatialCoordsFromSpher(spspher.spherpos).tocyl_sp_object()
        numpy.testing.assert_array_almost_equal(self.spcyl.cylpos, spback.cylpos)

    def test_cartesian_base(self):
        ex, ey, ez = self.spcyl.cartesian_base_vects_in_cyl_base()
        basealt = np.hstack((ex.transpose(), ey.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            posproj = BaseTools.project_on_orthonormed_direct_basis(self.randomcylposvectincylbase[i], indivbases[i])
            numpy.testing.assert_array_almost_equal(posproj[0], self.spcyl.xcart()[i])
            numpy.testing.assert_array_almost_equal(posproj[1], self.spcyl.ycart()[i])
            numpy.testing.assert_array_almost_equal(posproj[2], self.spcyl.zcart()[i])
            baseinv = BaseTools.invert_basis(indivbases[i])
            posprojback = BaseTools.project_on_orthonormed_direct_basis(posproj, baseinv)
            numpy.testing.assert_array_almost_equal(posprojback, self.randomcylposvectincylbase[i])

    def test_spherical_base(self):
        erspher, ethetaspher, ephi = self.spcyl.spherical_base_vects_in_cyl_base()
        basealt = np.hstack((erspher.transpose(), ethetaspher.transpose(), ephi.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            posproj = BaseTools.project_on_orthonormed_direct_basis(self.randomcylposvectincylbase[i], indivbases[i])
            numpy.testing.assert_array_almost_equal(posproj[0], self.spcyl.spherical_radius()[i])
            numpy.testing.assert_array_almost_equal(posproj[1], 0)
            numpy.testing.assert_array_almost_equal(posproj[2], 0)
            baseinv = BaseTools.invert_basis(indivbases[i])
            posprojback = BaseTools.project_on_orthonormed_direct_basis(posproj, baseinv)
            numpy.testing.assert_array_almost_equal(posprojback, self.randomcylposvectincylbase[i])


class TestSpatialCoordsFromSpher(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomspherposvect = np.zeros((self.nparts, 3))
        self.randomspherposvect[:, 0] = np.random.rand(self.nparts)
        self.randomspherposvect[:, 1] = np.random.rand(self.nparts) * np.pi
        self.randomspherposvect[:, 2] = np.random.rand(self.nparts) * 2 * np.pi - np.pi
        self.randomspherposvectinspherbase = np.copy(self.randomspherposvect)
        self.randomspherposvectinspherbase[:, 1] = 0
        self.randomspherposvectinspherbase[:, 2] = 0
        self.spspher = SpatialCoordsFromSpher(self.randomspherposvect)

    def test_cylindrical_radius(self):
        radc = self.spspher.cylindrical_radius()
        xcart = self.spspher.xcart()
        ycart = self.spspher.ycart()
        numpy.testing.assert_array_almost_equal(radc ** 2, xcart ** 2 + ycart ** 2)

    def test_phi(self):
        phi = self.spspher.phi()
        numpy.testing.assert_array_almost_equal(phi, self.randomspherposvect[:, 2])

    def test_tocart(self):
        cartpos = self.spspher.tocart_pos()
        numpy.testing.assert_array_almost_equal(cartpos[:, 0], self.spspher.xcart())
        numpy.testing.assert_array_almost_equal(cartpos[:, 1], self.spspher.ycart())
        numpy.testing.assert_array_almost_equal(cartpos[:, 2], self.spspher.zcart())
        spcart = SpatialCoordsFromCart(cartpos)
        spherpos = spcart.tospher_pos()
        numpy.testing.assert_array_almost_equal(spherpos, self.randomspherposvect)

    def test_tocart_sp_object(self):
        spcart = self.spspher.tocart_sp_object()
        spback = SpatialCoordsFromCart(spcart.pos).tospher_sp_object()
        numpy.testing.assert_array_almost_equal(self.spspher.spherpos, spback.spherpos)

    def test_tocyl(self):
        cylpos = self.spspher.tocyl_pos()
        numpy.testing.assert_array_almost_equal(cylpos[:, 0], self.spspher.cylindrical_radius())
        numpy.testing.assert_array_almost_equal(cylpos[:, 1], self.spspher.phi())
        numpy.testing.assert_array_almost_equal(cylpos[:, 2], self.spspher.zcart())
        spcyl = SpatialCoordsFromCyl(cylpos)
        spherpos = spcyl.tospher_pos()
        numpy.testing.assert_array_almost_equal(spherpos, self.randomspherposvect)

    def test_tocyl_sp_object(self):
        spcyl = self.spspher.tocyl_sp_object()
        spback = SpatialCoordsFromCyl(spcyl.cylpos).tospher_sp_object()
        numpy.testing.assert_array_almost_equal(self.spspher.spherpos, spback.spherpos)

    def test_cartesian_base(self):
        ex, ey, ez = self.spspher.cartesian_base_vects_in_spher_base()
        basealt = np.hstack((ex.transpose(), ey.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            posproj = BaseTools.project_on_orthonormed_direct_basis(self.randomspherposvectinspherbase[i],
                                                                    indivbases[i])
            numpy.testing.assert_array_almost_equal(posproj[0], self.spspher.xcart()[i])
            numpy.testing.assert_array_almost_equal(posproj[1], self.spspher.ycart()[i])
            numpy.testing.assert_array_almost_equal(posproj[2], self.spspher.zcart()[i])
            baseinv = BaseTools.invert_basis(indivbases[i])
            posprojback = BaseTools.project_on_orthonormed_direct_basis(posproj, baseinv)
            numpy.testing.assert_array_almost_equal(posprojback, self.randomspherposvectinspherbase[i])

    def test_cylindrical_base(self):
        ercyl, ephi, ez = self.spspher.cylindrical_base_vects_in_spher_base()
        basealt = np.hstack((ercyl.transpose(), ephi.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            posproj = BaseTools.project_on_orthonormed_direct_basis(self.randomspherposvectinspherbase[i],
                                                                    indivbases[i])
            numpy.testing.assert_array_almost_equal(posproj[0], self.spspher.cylindrical_radius()[i])
            numpy.testing.assert_array_almost_equal(posproj[1], 0)
            numpy.testing.assert_array_almost_equal(posproj[2], self.spspher.zcart()[i])
            baseinv = BaseTools.invert_basis(indivbases[i])
            posprojback = BaseTools.project_on_orthonormed_direct_basis(posproj, baseinv)
            numpy.testing.assert_array_almost_equal(posprojback, self.randomspherposvectinspherbase[i])


class TestVelCoordsFromCart(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomposvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randomvelvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randommass = np.random.rand(self.nparts)
        self.velc = VelCoordsFromCart(self.randomposvect, self.randomvelvect)

    def test_norm_of_vel(self):
        norm_of_vel = self.velc.norm_of_vel()
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2,
                                                self.randomvelvect[:, 0] ** 2 + self.randomvelvect[:, 1] ** 2 +
                                                self.randomvelvect[:, 2] ** 2)

    def test_disps_vels(self):
        av_vx, av_vy, av_vz = self.velc.average_vel_coords(self.randommass)
        dispx, dispy, dispz = self.velc.disp_of_vel(self.randommass)
        numpy.testing.assert_array_almost_equal(BaseTools.average64_1d(self.velc.norm_of_vel() ** 2, self.randommass),
                                                dispx ** 2 + dispy ** 2 + dispz ** 2
                                                + av_vx ** 2 + av_vy ** 2 + av_vz ** 2)

    def test_all_vels(self):
        spher_rad_vel = self.velc.spherical_radial_vel()
        phi_vel = self.velc.phi_vel()
        theta_vel = self.velc.spherical_theta_vel()
        cyl_rad_vel = self.velc.cylindrical_radial_vel()
        z_vel = self.randomvelvect[:, 2]
        norm_of_vel = self.velc.norm_of_vel()
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2, spher_rad_vel ** 2 + theta_vel ** 2 + phi_vel ** 2)
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2, cyl_rad_vel ** 2 + phi_vel ** 2 + z_vel ** 2)

    def test_tocyl(self):
        cylpos = self.velc.tocyl_pos()
        cylvel = self.velc.tocyl_vel()
        numpy.testing.assert_array_almost_equal(cylvel[:, 0], self.velc.cylindrical_radial_vel())
        numpy.testing.assert_array_almost_equal(cylvel[:, 1], self.velc.phi_vel())
        numpy.testing.assert_array_almost_equal(cylvel[:, 2], self.randomvelvect[:, 2])
        velccyl = VelCoordsFromCyl(cylpos, cylvel)
        cartvel = velccyl.tocart_vel()
        numpy.testing.assert_array_almost_equal(cartvel, self.randomvelvect)

    def test_tospher(self):
        spherpos = self.velc.tospher_pos()
        sphervel = self.velc.tospher_vel()
        numpy.testing.assert_array_almost_equal(sphervel[:, 0], self.velc.spherical_radial_vel())
        numpy.testing.assert_array_almost_equal(sphervel[:, 1], self.velc.spherical_theta_vel())
        numpy.testing.assert_array_almost_equal(sphervel[:, 2], self.velc.phi_vel())
        velcspher = VelCoordsFromSpher(spherpos, sphervel)
        cartvel = velcspher.tocart_vel()
        numpy.testing.assert_array_almost_equal(cartvel, self.randomvelvect)


class TestVelCoordsFromCyl(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomcylposvect = np.zeros((self.nparts, 3))
        self.randomcylposvect[:, 0] = np.random.rand(self.nparts)
        self.randomcylposvect[:, 1] = np.random.rand(self.nparts) * 2 * np.pi - np.pi
        self.randomcylposvect[:, 2] = np.random.rand(self.nparts) * 2 - 1.
        self.randomcylvelvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randommass = np.random.rand(self.nparts)
        self.velc = VelCoordsFromCyl(self.randomcylposvect, self.randomcylvelvect)

    def test_norm_of_vel(self):
        norm_of_vel = self.velc.norm_of_vel()
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2,
                                                self.randomcylvelvect[:, 0] ** 2 + self.randomcylvelvect[:, 1] ** 2 +
                                                self.randomcylvelvect[:, 2] ** 2)

    def test_disps_vels(self):
        av_vx, av_vy, av_vz = self.velc.average_vel_coords(self.randommass)
        dispx, dispy, dispz = self.velc.disp_of_vel(self.randommass)
        numpy.testing.assert_array_almost_equal(BaseTools.average64_1d(self.velc.norm_of_vel() ** 2, self.randommass),
                                                dispx ** 2 + dispy ** 2 + dispz ** 2
                                                + av_vx ** 2 + av_vy ** 2 + av_vz ** 2)

    def test_all_vels(self):
        spher_rad_vel = self.velc.spherical_radial_vel()
        theta_vel = self.velc.spherical_theta_vel()
        vel_xcart = self.velc.vel_xcart()
        vel_ycart = self.velc.vel_ycart()
        phi_vel = self.randomcylvelvect[:, 1]
        z_vel = self.randomcylvelvect[:, 2]
        norm_of_vel = self.velc.norm_of_vel()
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2, spher_rad_vel ** 2 + theta_vel ** 2 + phi_vel ** 2)
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2, vel_xcart ** 2 + vel_ycart ** 2 + z_vel ** 2)

    def test_tospher(self):
        spherpos = self.velc.tospher_pos()
        sphervel = self.velc.tospher_vel()
        numpy.testing.assert_array_almost_equal(sphervel[:, 0], self.velc.spherical_radial_vel())
        numpy.testing.assert_array_almost_equal(sphervel[:, 1], self.velc.spherical_theta_vel())
        numpy.testing.assert_array_almost_equal(sphervel[:, 2], self.randomcylvelvect[:, 1])
        velcspher = VelCoordsFromSpher(spherpos, sphervel)
        cylvel = velcspher.tocyl_vel()
        numpy.testing.assert_array_almost_equal(cylvel, self.randomcylvelvect)

    def test_tocart(self):
        cartpos = self.velc.tocart_pos()
        cartvel = self.velc.tocart_vel()
        numpy.testing.assert_array_almost_equal(cartvel[:, 0], self.velc.vel_xcart())
        numpy.testing.assert_array_almost_equal(cartvel[:, 1], self.velc.vel_ycart())
        numpy.testing.assert_array_almost_equal(cartvel[:, 2], self.randomcylvelvect[:, 2])
        velccart = VelCoordsFromCart(cartpos, cartvel)
        cylvel = velccart.tocyl_vel()
        numpy.testing.assert_array_almost_equal(cylvel, self.randomcylvelvect)


class TestVelCoordsFromSpher(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomspherposvect = np.zeros((self.nparts, 3))
        self.randomspherposvect[:, 0] = np.random.rand(self.nparts)
        self.randomspherposvect[:, 1] = np.random.rand(self.nparts) * np.pi
        self.randomspherposvect[:, 2] = np.random.rand(self.nparts) * 2 * np.pi - np.pi
        self.randomspherposvectinspherbase = np.copy(self.randomspherposvect)
        self.randomspherposvectinspherbase[:, 1] = 0
        self.randomspherposvectinspherbase[:, 2] = 0
        self.randomsphervelvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randommass = np.random.rand(self.nparts)
        self.velc = VelCoordsFromSpher(self.randomspherposvect, self.randomsphervelvect)

    def test_norm_of_vel(self):
        norm_of_vel = self.velc.norm_of_vel()
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2,
                                                self.randomsphervelvect[:, 0] ** 2 + self.randomsphervelvect[:, 1] ** 2 +
                                                self.randomsphervelvect[:, 2] ** 2)

    def test_disps_vels(self):
        av_vx, av_vy, av_vz = self.velc.average_vel_coords(self.randommass)
        dispx, dispy, dispz = self.velc.disp_of_vel(self.randommass)
        numpy.testing.assert_array_almost_equal(BaseTools.average64_1d(self.velc.norm_of_vel() ** 2, self.randommass),
                                                dispx ** 2 + dispy ** 2 + dispz ** 2
                                                + av_vx ** 2 + av_vy ** 2 + av_vz ** 2)

    def test_all_vels(self):
        vel_xcart = self.velc.vel_xcart()
        vel_ycart = self.velc.vel_ycart()
        vel_zcart = self.velc.vel_zcart()
        cyl_rad_vel = self.velc.cylindrical_radial_vel()
        norm_of_vel = self.velc.norm_of_vel()
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2,
                                                cyl_rad_vel ** 2 + self.randomsphervelvect[:, 2] ** 2 + vel_zcart ** 2)
        numpy.testing.assert_array_almost_equal(norm_of_vel ** 2, vel_xcart ** 2 + vel_ycart ** 2 + vel_zcart ** 2)

    def test_tocart(self):
        cartpos = self.velc.tocart_pos()
        cartvel = self.velc.tocart_vel()
        numpy.testing.assert_array_almost_equal(cartvel[:, 0], self.velc.vel_xcart())
        numpy.testing.assert_array_almost_equal(cartvel[:, 1], self.velc.vel_ycart())
        numpy.testing.assert_array_almost_equal(cartvel[:, 2], self.velc.vel_zcart())
        velccart = VelCoordsFromCart(cartpos, cartvel)
        sphervel = velccart.tospher_vel()
        numpy.testing.assert_array_almost_equal(sphervel, self.randomsphervelvect)

    def test_tocyl(self):
        cylpos = self.velc.tocyl_pos()
        cylvel = self.velc.tocyl_vel()
        numpy.testing.assert_array_almost_equal(cylvel[:, 0], self.velc.cylindrical_radial_vel())
        numpy.testing.assert_array_almost_equal(cylvel[:, 1], self.randomsphervelvect[:, 2])
        numpy.testing.assert_array_almost_equal(cylvel[:, 2], self.velc.vel_zcart())
        velccyl = VelCoordsFromCyl(cylpos, cylvel)
        sphervel = velccyl.tospher_vel()
        numpy.testing.assert_array_almost_equal(sphervel, self.randomsphervelvect)


class TestDynamicsFromCart(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomposvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randomvelvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randommass = np.random.rand(self.nparts)
        self.dyn = DynamicsFromCart(self.randomposvect, self.randomvelvect, self.randommass)

    def test_angular_momentum_3d(self):
        jang_3d = self.dyn.angular_momentum_3d()
        numpy.testing.assert_array_almost_equal(np.zeros((self.nparts)),
                                                np.diag(np.dot(self.dyn.pos, np.transpose(jang_3d))))
        numpy.testing.assert_array_almost_equal(np.zeros((self.nparts)),
                                                np.diag(np.dot(self.dyn.vel, np.transpose(jang_3d))))


class TestDynamicsFromCyl(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomcylposvect = np.zeros((self.nparts, 3))
        self.randomcylposvect[:, 0] = np.random.rand(self.nparts)
        self.randomcylposvect[:, 1] = np.random.rand(self.nparts) * 2 * np.pi - np.pi
        self.randomcylposvect[:, 2] = np.random.rand(self.nparts) * 2 - 1.
        self.randomvelvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randommass = np.random.rand(self.nparts)
        self.dyncyl = DynamicsFromCyl(self.randomcylposvect, self.randomvelvect, self.randommass)

    def test_angular_momentum_3d(self):
        jang_3d_cyl = self.dyncyl.angular_momentum_3d()
        numpy.testing.assert_array_almost_equal(np.zeros((self.nparts)),
                                                np.diag(np.dot(self.dyncyl.posrz, np.transpose(jang_3d_cyl))))
        numpy.testing.assert_array_almost_equal(np.zeros((self.nparts)),
                                                np.diag(np.dot(self.dyncyl.vel, np.transpose(jang_3d_cyl))))
        dyncart = self.dyncyl.tocart_dyn_obj()
        jang_3d_from_cart = dyncart.angular_momentum_3d()
        ex, ey, ez = self.dyncyl.cartesian_base_vects_in_cyl_base()
        basealt = np.hstack((ex.transpose(), ey.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            jang_3d_cyl_proj_on_cart = BaseTools.project_on_orthonormed_direct_basis(
                jang_3d_cyl[i], indivbases[i])
            numpy.testing.assert_array_almost_equal(jang_3d_cyl_proj_on_cart, jang_3d_from_cart[i])


class TestDynamicsFromSpher(unittest.TestCase):
    def setUp(self):
        self.nparts = 10
        self.randomspherposvect = np.zeros((self.nparts, 3))
        self.randomspherposvect[:, 0] = np.random.rand(self.nparts)
        self.randomspherposvect[:, 1] = np.random.rand(self.nparts) * np.pi
        self.randomspherposvect[:, 2] = np.random.rand(self.nparts) * 2 * np.pi - np.pi
        self.randomvelvect = np.random.rand(self.nparts, 3) * 2 - 1.
        self.randommass = np.random.rand(self.nparts)
        self.dynspher = DynamicsFromSpher(self.randomspherposvect, self.randomvelvect, self.randommass)

    def test_angular_momentum_3d(self):
        jang_3d_spher = self.dynspher.angular_momentum_3d()
        numpy.testing.assert_array_almost_equal(np.zeros((self.nparts)),
                                                np.diag(np.dot(self.dynspher.posr, np.transpose(jang_3d_spher))))
        numpy.testing.assert_array_almost_equal(np.zeros((self.nparts)),
                                                np.diag(np.dot(self.dynspher.vel, np.transpose(jang_3d_spher))))
        dyncart = self.dynspher.tocart_dyn_obj()
        jang_3d_from_cart = dyncart.angular_momentum_3d()
        ex, ey, ez = self.dynspher.cartesian_base_vects_in_spher_base()
        basealt = np.hstack((ex.transpose(), ey.transpose(), ez.transpose()))
        indivbases = np.reshape(basealt, (self.nparts, 3, 3))
        for i in range(0, self.nparts):
            self.assertTrue(BaseTools.check_ortho_normed_direct_basis(indivbases[i]))
            jang_3d_spher_proj_on_cart = BaseTools.project_on_orthonormed_direct_basis(
                jang_3d_spher[i], indivbases[i])
            numpy.testing.assert_array_almost_equal(jang_3d_spher_proj_on_cart, jang_3d_from_cart[i])
