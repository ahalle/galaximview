galaximview.basefuncs module
------------------------------

Functions used by other modules.

.. automodule:: galaximview.basefuncs
   :members:
   :undoc-members:
   :show-inheritance:

      
