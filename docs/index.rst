.. galaximview documentation master file, created by
   sphinx-quickstart on Thu Apr  1 15:18:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation of GalaXimView
=============================

GalaXimView (for Galaxies Simulations Viewer) is designed to visualise simulations using particles, providing notably a rotatable 3D view and corresponding projections in 2D, together with a way of navigating through snapshots of a simulation keeping the same projection.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   overview
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
