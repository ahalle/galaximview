from matplotlib import colors as mcolors
from .snap import *
from matplotlib.widgets import Button


class GalViewer:
    """Particles simulations viewer to rotate particles, project them (2D surface density and mass-weighted quantities.)
    You can rotate the 3D view and obtain the 2D projection by pressing the "Set View" button. You can zoom in or out
    using your graphics backend usual buttons on the 2D projection and replot the 2D projection with the same number
    of bins as before zooming by pressing the 'Set View' button.

    Parameters
    ----------
    sim_obj : IterOnSim
        Simulation object, of class IterOnSim.
    step : int, default = 1
        Interval between snapshots when in viewer.
    ibeg : int or `None`, default = `None`
        Number of first snapshot viewed in viewer. If is `None` set to minimum snapshot number in simulation directory.
    compstr : str or `None`, default = `None`
        Member of Snap.listcomps that viewer starts showing. If is `None`, the viewer selects an existing component.
    frac_3d_ini : float in [0,1], default = 0.05
        Fraction of particles shown in the 3D plot, between 0 and 1. Defaults to 0.05, i.e. 5%.
    adaptive_zoom_ini : bool, default = `True`
        If `True`, adapts limits of 2D and 3D plots to enclose most particles.
    rmax : float, optional
        Size of initial 3D box and 2D projection if adaptive_zoom_ini is False. If adaptive_zoom_ini is False,
        3D plot has x, y, and z in [-rmax, rmax] and 2D projection has x_projected and y_projected in [-rmax, rmax].
        In default configuration, adpative_zoom_ini is True.
    nbins : int, default = 256
        Initial number of bins in x_projected, for 2D projection.
    ids : int ndarray, optional
        Specific particles identifiers (not fully developped).
    seed : int, optional
        Specific eed for random generator, can be set to specific natural integer for reproduction.
    figname : str, default = 'GalaXimView'
        Name of matplotlib figure.
    coords_conv_type : {'astro', cgs', `None`}
        Type of units conversion for spatial coordinates.
    global_conv_type : {'astro', cgs', `None`}
        Type of units conversion.
    retrieve_current_2d_hist : bool, default = `False`
        `True` to store current 2D histogram and edges in self.current_2d_hist,
        self.current 2d_yedges and self.current 2d_xedges.

    Attributes
    ----------
    simu:
        Simulation object.
    snap:
        Current snapshot object.
    """

    def __init__(self, sim_obj, step=1, ibeg=None, compstr=None, frac_3d_ini=0.05, adaptive_zoom_ini=True, rmax=60.,
                 nbins=256, ids=None, seed=None, figname='GalaXimView', coords_conv_type='astro',
                 global_conv_type='astro', retrieve_current_2d_hist=False):

        # set simulation object, first snapshot object and time step
        self.simu = sim_obj
        self.ibeg = self.simu.isnapmin if ibeg is None else ibeg
        self.snap = self.simu.get_snap(self.ibeg)
        self.step = 1 if step is None else step
        self.step_ini = 1 if step is None else step

        # set galaxy component shown on plots
        self.compstr = self.snap.listindivcomps[0] if (compstr is None) else compstr
        self.list_comps = [self.compstr]

        # set units conversion
        self.coords_conv_type = coords_conv_type  # for spatial coordinates
        self.fac_conv_pos = BaseUnitsDefs.get_conversion_factor_from_string('xpos', self.snap.dicunits,
                                                                            conv_type=self.coords_conv_type)
        self.global_conv_type = global_conv_type  # for various plotted quantities

        # set x, y and z coordinates for 3D plot (projection in 2D depends on angle with which 3D plot is viewed.)
        self.xstr = 'xpos'
        self.ystr = 'ypos'
        self.zstr = 'zpos'

        # set fraction of particles of components shown in 3D
        self.frac = frac_3d_ini
        self.frac_ini = frac_3d_ini

        # set seed used to generate subsets of particles in 3D
        self.seed = seed

        # set number of bins for 2D plot
        self.nb = nbins
        self.nb_ini = nbins

        self.ids = ids
        self.plot_recent_sf = False
        self.substract_av_value = False
        self.vect_ctr_inv = np.array([0, 0, 0])
        self.fig = plt.figure(figname)
        fig_manager = plt.get_current_fig_manager()
        if hasattr(fig_manager, 'window'):
            if hasattr(fig_manager.window, 'maximize'):
                fig_manager.window.maximize()
            else:
                self.fig.set_size_inches(np.array([18.48, 9.75]))
        else:
            self.fig.set_size_inches(np.array([18.48, 9.75]))
        self.figgs = self.fig.add_gridspec(15, 20)
        self.ax3d = self.fig.add_subplot(1, 2, 1, projection='3d')
        self.ax2d = self.fig.add_subplot(1, 2, 2)
        self.dic_cols = {'gas': 'C1', 'all stars': 'C2', 'disc stars': 'C7', 'bulge stars': 'C3', 'old stars': 'C0',
                         'new stars': 'C4', 'DM': 'C5'}
        self.saved_elev = 30
        self.saved_azim = -90
        self.xlimit_densofr = None
        self.ylimit_densofr = None
        self.xlimit_velofr = None
        self.ylimit_velofr = None
        self.xlimit_dispvofr = None
        self.ylimit_dispvofr = None
        self.rmax_curr_3d = rmax
        if adaptive_zoom_ini:
            xmax = np.amax(self.snap.get_array_from_string(self.xstr, self.snap.get_indexes_of_component('total')))
            xmin = np.amin(self.snap.get_array_from_string(self.xstr, self.snap.get_indexes_of_component('total')))
            ymax = np.amax(self.snap.get_array_from_string(self.ystr, self.snap.get_indexes_of_component('total')))
            ymin = np.amin(self.snap.get_array_from_string(self.ystr, self.snap.get_indexes_of_component('total')))
            rmax = max(np.fabs(xmax), np.fabs(xmin), np.fabs(ymax), np.fabs(ymin))
            rmax *= self.fac_conv_pos
            self.xlim2d = (-rmax, rmax)
            self.ylim2d = (-rmax, rmax)
            self.xlimit_3d = (-rmax, rmax)
            self.ylimit_3d = (-rmax, rmax)
            self.zlimit_3d = (-rmax, rmax)
            self.plot_comps_in_3d(initial_plot=False, xlimit=self.xlimit_3d, ylimit=self.ylimit_3d,
                                  zlimit=self.zlimit_3d,
                                  elev=self.saved_elev, azim=self.saved_azim)
        else:
            self.xlim2d = (-rmax, rmax)
            self.ylim2d = (-rmax, rmax)
            self.plot_comps_in_3d(initial_plot=True, elev=self.saved_elev, azim=self.saved_azim)
            self.xlimit_3d = None
            self.ylimit_3d = None
            self.zlimit_3d = None
        self.rmax = rmax
        self.rmax_curr_2d = rmax
        self.match_3d_view()
        self.cbar_lims = None
        self.cic_hists = False
        self.add_eps_in_plot = True
        self.compstr_hist = self.compstr
        self.select_stars_age = False
        self.xlimsfr = None
        self.timelimsfr = None

        self.xlimurho = None
        self.ylimurho = None
        self.axurho = None
        self.axurhoxz = None
        self.axurhozy = None
        self.select_gas_u_nh = False

        self.plotted_2d = 'surf'
        self.sph_on = False
        self.plotted_1d = 'PV'
        self.new_1d_geom = True
        self.retrieve_current_2d_hist = retrieve_current_2d_hist
        self.current_2d_hist = None
        self.current_2d_yedges = None
        self.current_2d_xedges = None

        self.plot_2d(self.compstr, initial_plot=True)

        # set mass box (top left corner)
        self.massbox = self.fig.add_axes([0.005, 0.995, 0.1, 0.1])
        self.write_masses()

        # set printing of step, min and max snapshot numbers
        self.stepboxminus = self.fig.add_axes([0.765, 0.9625, 0.1, 0.1])
        self.stepboxplus = self.fig.add_axes([0.7925, 0.9625, 0.1, 0.1])
        self.write_step()
        axminsnap = self.fig.add_axes([0.74, 0.9625, 0.1, 0.1])
        axminsnap.axis('off')
        axmaxsnap = self.fig.add_axes([0.82, 0.9625, 0.1, 0.1])
        axmaxsnap.axis('off')
        axminsnap.text(0.01, 0.01, str(self.simu.isnapmin), va='top', ha='center')
        axmaxsnap.text(0.01, 0.01, str(self.simu.isnapmax), va='top', ha='center')

        # set printing of fraction of particles plotted in 3D
        self.fracbox = self.fig.add_axes([0.375, 0.99, 0.1, 0.1])
        self.write_frac()

        self.quiver_plot = False
        self.basis_los = get_basis_los(self.ax3d.azim, self.ax3d.elev)
        self.just_3d = True
        self.axdensofr = None
        self.axvelofr = None
        self.axdispvofr = None
        axsetsaveelev = self.fig.add_axes([0.1625, 0.9625, 0.1, 0.1])
        axsetsaveelev.axis('off')
        axsetsaveazim = self.fig.add_axes([0.2325, 0.9625, 0.1, 0.1])
        axsetsaveazim.axis('off')
        axsetsaveelev.text(0.01, 0.01, '->', va='top', ha='center')
        axsetsaveazim.text(0.01, 0.01, '->', va='top', ha='center')

    def write_masses(self):
        """Fetches and writes masses of components in text box."""
        self.massbox.cla()
        self.massbox.axis('off')
        dic_of_masses = self.snap.get_dic_of_components_masses()
        dic_of_comps_to_look_up = {'total in m sun': 'Total: ', 'gas in m sun': 'Gas: ',
                                   'all stars in m sun': 'All *: ', 'disc stars in m sun': 'Disc: ',
                                   'bulge stars in m sun': 'Bulge: ', 'new stars in m sun': 'New *: ',
                                   'DM in m sun': 'DM: '}
        strmasses = r'Masses in $10^{10}$ M$_{\odot}$ :' + ' \n'
        for compstr in dic_of_comps_to_look_up.keys():
            if compstr in dic_of_masses.keys():
                mass = np.around(dic_of_masses[compstr] / 1e10, 2)
                strmasses += dic_of_comps_to_look_up[compstr] + str(mass) + '\n'
        self.massbox.text(0.01, 0.01, strmasses, va='top')

    def write_step(self):
        """Fetches and writes time step."""
        self.stepboxminus.cla()
        self.stepboxminus.axis('off')
        strstep = '- ' + str(self.step)
        self.stepboxminus.text(0.01, 0.01, strstep, va='top', ha='center')
        self.stepboxplus.cla()
        self.stepboxplus.axis('off')
        strstep = '+ ' + str(self.step)
        self.stepboxplus.text(0.01, 0.01, strstep, va='top', ha='center')

    def write_frac(self):
        """Fetches and writes fraction of particles plotted in 3D."""
        self.fracbox.cla()
        self.fracbox.axis('off')
        strfrac = str(self.frac)
        self.fracbox.text(0.01, 0.01, strfrac, va='top', ha='center')

    def plot_comps_in_3d(self, initial_plot=False, xlimit=None, ylimit=None, zlimit=None, elev=None, azim=None):
        """Plots 3D plot. self.snap.posini, i.e. the untouched positions, are used with a possible translation if
        plot's "zero-point" is modified by self.match_2d_view.

        Parameters
        ----------
        initial_plot : bool, default = ` False`
            `True` if method plots the 3D view for the first time with no adaptive initial zoom,
            in self.__init__ (Default value = False)
        xlimit : (float, float) or None, default = None
            Limits of x axis. If None, limits are obtained from current axis self.ax3d.
        ylimit : (float, float) or None, default = None
            Limits of y axis. If None, limits are obtained from current axis self.ax3d.
        zlimit : (float, float) or None, default = None
            Limits of z axis. If None, limits are obtained from current axis self.ax3d.
        elev : float or None, optional
            Elevation of matplotlib camera in 3D plot used for first plotting. If None, set to matplotlib default.
        azim :
            Azimuth of matplotlib camera in 3D plot used for first plotting. If None, set to matplotlib default.
        """
        if not initial_plot:
            self.rmax_curr_3d = self.ax3d.get_xlim()[1]
        self.ax3d.cla()
        if (elev is not None):
            self.ax3d.view_init(elev=elev, azim=azim)
        if len(self.list_comps) == 0:
            plot_3d(np.array([0]), np.array([0]), np.array([0]), (-self.rmax_curr_3d, self.rmax_curr_3d),
                    (-self.rmax_curr_3d, self.rmax_curr_3d), (-self.rmax_curr_3d, self.rmax_curr_3d), ax=self.ax3d)
        else:
            if ((xlimit is None) & (ylimit is None) & (zlimit is None)):
                xlimit = ylimit = zlimit = (-self.rmax_curr_3d, self.rmax_curr_3d)

            if self.ids is not None:
                inds = self.snap.get_indexes_from_ids(self.ids)
                self.snap.plot_3d_for_subset('total', self.frac, xstr=self.xstr + '_ini', ystr=self.ystr + '_ini',
                                             zstr=self.zstr + '_ini', xlimit=xlimit, ylimit=ylimit, zlimit=zlimit,
                                             inds=inds, seed=self.seed,
                                             zero_point=self.vect_ctr_inv, ax=self.ax3d)
            else:
                for compstr in self.list_comps:
                    if compstr in self.snap.listcomps:
                        self.snap.plot_3d_for_subset(compstr, self.frac, xstr=self.xstr + '_ini',
                                                     ystr=self.ystr + '_ini',
                                                     zstr=self.zstr + '_ini', xlimit=xlimit, ylimit=ylimit,
                                                     zlimit=zlimit, seed=self.seed,
                                                     zero_point=self.vect_ctr_inv,
                                                     ax=self.ax3d, col=self.dic_cols[compstr])
        if np.any(self.vect_ctr_inv):
            self.ax3d.set_xlabel('Recentred ' + self.ax3d.get_xlabel())
            self.ax3d.set_ylabel('Recentred ' + self.ax3d.get_ylabel())
            self.ax3d.set_zlabel('Recentred ' + self.ax3d.get_zlabel())

        self.ax3d.patch.set_alpha(0)

    def match_2d_view(self):
        """Centres 3D plot in xc, yc, zc with xc the middle of the x_projected axis, yc the middle of the y_projected)
        axis, and zc the mass-weighted average of the z position, on a z axis orthogonal to the projection plane, of
        particles (of all types) that can be viewed in the 2D projection.
        """
        inds = np.where((self.snap.data['pos'][:, 0] * self.fac_conv_pos < self.xlim2d[1]) & (
                self.snap.data['pos'][:, 0] * self.fac_conv_pos > self.xlim2d[0]) & (
                                self.snap.data['pos'][:, 1] * self.fac_conv_pos > self.ylim2d[0]) & (
                                self.snap.data['pos'][:, 1] * self.fac_conv_pos > self.ylim2d[0]))
        avz = np.average(self.snap.data['pos'][inds[0], 2] * self.fac_conv_pos, weights=self.snap.data['mass'][inds[0]])
        avx = 0.5 * (self.xlim2d[0] + self.xlim2d[1])
        avy = 0.5 * (self.ylim2d[0] + self.ylim2d[1])
        vect_ctr = np.array([avx, avy, avz])
        inv_basis_los = BaseTools.invert_basis(self.basis_los)
        self.vect_ctr_inv = BaseTools.project_on_orthonormed_direct_basis(vect_ctr, inv_basis_los)
        sidex = (self.xlim2d[1] - self.xlim2d[0])
        sidey = (self.ylim2d[1] - self.ylim2d[0])
        sidemax = max(sidex, sidey)
        xlimit = ylimit = zlimit = (- 0.5 * sidemax, + 0.5 * sidemax)
        self.plot_comps_in_3d(xlimit=xlimit, ylimit=ylimit, zlimit=zlimit)

    def match_3d_view(self):
        """Projects positions and velocities on the right basis to obtain projection. This actually modifies
        self.snap.data['pos'] and self.snap.data['vel']. Untouched positions and velocities are stored in
        self.snap.posini and self.snap.velini.
        """
        self.basis_los = get_basis_los(self.ax3d.azim, self.ax3d.elev)
        self.snap.data['pos'] = BaseTools.project_on_orthonormed_direct_basis(self.snap.posini, self.basis_los)
        self.snap.data['vel'] = BaseTools.project_on_orthonormed_direct_basis(self.snap.velini, self.basis_los)

    def lims_from3d(self):
        """Finds coordinates limits to use in 2D projection if button 'Zoom 3D' is pressed.
        Returns
        -------
        (float, float), (float, float)
            x and y limits to use in 2D projection.
        """
        xlim = self.ax3d.get_xlim()
        ylim = self.ax3d.get_ylim()
        zlim = self.ax3d.get_zlim()
        xav = 0.5 * (xlim[0] + xlim[1])
        yav = 0.5 * (ylim[0] + ylim[1])
        zav = 0.5 * (zlim[0] + zlim[1])
        ecks = np.zeros((8, 3))
        ecks[0, :] = np.array([xlim[0] - xav, ylim[0] - yav, zlim[0] - zav])
        ecks[1, :] = np.array([xlim[0] - xav, ylim[1] - yav, zlim[0] - zav])
        ecks[2, :] = np.array([xlim[1] - xav, ylim[1] - yav, zlim[0] - zav])
        ecks[3, :] = np.array([xlim[1] - xav, ylim[0] - yav, zlim[0] - zav])
        ecks[4, :] = np.array([xlim[0] - xav, ylim[0] - yav, zlim[1] - zav])
        ecks[5, :] = np.array([xlim[0] - xav, ylim[1] - yav, zlim[1] - zav])
        ecks[6, :] = np.array([xlim[1] - xav, ylim[1] - yav, zlim[1] - zav])
        ecks[7, :] = np.array([xlim[1] - xav, ylim[0] - yav, zlim[1] - zav])
        ecks_proj = BaseTools.project_on_orthonormed_direct_basis(ecks, self.basis_los)
        xmin = np.amin(ecks_proj[:, 0])
        xmax = np.amax(ecks_proj[:, 0])
        ymin = np.amin(ecks_proj[:, 1])
        ymax = np.amax(ecks_proj[:, 1])
        xlim2d = (xmin, xmax)
        ylim2d = (ymin, ymax)
        vect_ctr = BaseTools.project_on_orthonormed_direct_basis(self.vect_ctr_inv, self.basis_los)
        xlim2d += vect_ctr[0]
        ylim2d += vect_ctr[1]
        return xlim2d, ylim2d

    def plot_2d(self, compstr, initial_plot=False, zoom_ini=False, zoom_3d=False):
        """Plots projected surface density or mass-weighted quantity.

        Parameters
        ----------
        compstr : str
            Member of self.snap.listcomps.
        initial_plot : bool, default = ` False`
            `True` if method plots the 2D view for the first time with no adaptive initial zoom,
            in self.__init__
        zoom_ini : bool, default = ` False`
            `True` to get back to initial zoom, set to `True` by pressing button 'Zoom ini".
        zoom_3d : bool, default = ` False`
            `True` to get 2D projection with limits set to correspond to 3D view, by self.lims_from3s.
            Set to `True` by pressing button 'Zoom 3D".
        """

        plt.sca(self.ax2d)
        if initial_plot is False:
            self.xlim2d = self.ax2d.get_xlim()
            self.ylim2d = self.ax2d.get_ylim()
            if zoom_ini:
                self.xlim2d = self.ylim2d = (-self.rmax, self.rmax)
            if zoom_3d:
                self.xlim2d, self.ylim2d = self.lims_from3d()

        # to avoid superposition of 2d plot with buttons
        sizey = self.ylim2d[1] - self.ylim2d[0]
        midy = 0.5 * (self.ylim2d[1] + self.ylim2d[0])
        sizex = self.xlim2d[1] - self.xlim2d[0]
        sizeylim = 1.1 * sizex
        if (sizey > sizeylim):
            self.ylim2d = (midy - 0.5 * sizeylim, midy + 0.5 * sizeylim)

        if initial_plot is False:
            if np.size(plt.gca().images):
                plt.gca().images[0].colorbar.remove()
        self.ax2d.cla()
        if self.cbar_lims is not None:
            vmin = self.cbar_lims[0]
            vmax = self.cbar_lims[1]
        else:
            vmin = vmax = None

        timeingyr = self.snap.data['time'] * BaseUnitsDefs.get_conversion_factor_from_string('time', self.snap.dicunits,
                                                                                             conv_type='astro')
        strtit = 't=' + str(np.around(timeingyr, 2)) + ' Gyr, ' + self.compstr_hist

        # check there is something to show in frame
        if self.ids is not None:
            inds = self.snap.get_indexes_from_ids(self.ids)
        else:
            inds = self.snap.get_indexes_of_component(compstr)
        inds = np.where((self.snap.data['pos'][inds, 0] * self.fac_conv_pos < self.xlim2d[1]) & (
                self.snap.data['pos'][inds, 0] * self.fac_conv_pos > self.xlim2d[0]) & (
                                self.snap.data['pos'][inds, 1] * self.fac_conv_pos < self.ylim2d[1]) & (
                                self.snap.data['pos'][inds, 1] * self.fac_conv_pos > self.ylim2d[0]))[0]
        if np.size(inds) == 0:
            strtit += ' Nothing to show here.'
            plt.title(strtit)
            return 0
        elif ((compstr == 'new stars') & (self.select_stars_age)):
            inds = self.snap.select_new_stars_indexes_with_tbirth(self.timelimsfr, conv_type='astro')
            if np.size(inds) == 0:
                strtit += ' Nothing to show here.'
                plt.title(strtit)
                return 0
        else:
            if self.ids is None:
                inds = None
            else:
                inds = self.snap.get_indexes_from_ids(self.ids)

        if self.plotted_2d == 'sigma v_los':
            if (compstr == 'gas') & (self.sph_on):
                h2d, edy, edx = \
                    self.snap.sph_binned_dispersion_of_quantity_in_2d(compstr, self.xstr, self.ystr, '-zvel', inds=inds,
                                                                      x_conv_type=self.coords_conv_type,
                                                                      y_conv_type=self.coords_conv_type,
                                                                      xlimit=self.xlim2d,
                                                                      ylimit=self.ylim2d, nbx=self.nb,
                                                                      quantity_conv_type=self.global_conv_type,
                                                                      funcforpl=identity_function, vmin_plot=vmin,
                                                                      vmax_plot=vmax,
                                                                      add_eps_in_plot=self.add_eps_in_plot,
                                                                      fig=self.fig, cb_loc=self.ax2d)
            else:
                h2d, edy, edx = \
                    self.snap.binned_dispersion_of_quantity_in_2d(compstr, self.xstr, self.ystr, '-zvel', inds=inds,
                                                                  x_conv_type=self.coords_conv_type,
                                                                  y_conv_type=self.coords_conv_type,
                                                                  xlimit=self.xlim2d, ylimit=self.ylim2d,
                                                                  nbx=self.nb, weight_type='mass',
                                                                  quantity_conv_type=self.global_conv_type,
                                                                  funcforpl=identity_function, vmin_plot=vmin,
                                                                  vmax_plot=vmax, add_eps_in_plot=False,
                                                                  fig=self.fig, cb_loc=self.ax2d)
        elif self.plotted_2d == 'v los':
            if (compstr == 'gas') & (self.sph_on):
                h2d, edy, edx = \
                    self.snap.sph_binned_quantity_in_2d(compstr, self.xstr, self.ystr, '-zvel', inds=inds,
                                                        x_conv_type=self.coords_conv_type,
                                                        y_conv_type=self.coords_conv_type, xlimit=self.xlim2d,
                                                        ylimit=self.ylim2d, nbx=self.nb,
                                                        quantity_conv_type=self.global_conv_type,
                                                        funcforpl=identity_function, vmin_plot=vmin, vmax_plot=vmax,
                                                        add_eps_in_plot=self.add_eps_in_plot,
                                                        fig=self.fig, cb_loc=self.ax2d)
            else:
                h2d, edy, edx = self.snap.binned_quantity_in_2d(compstr, self.xstr, self.ystr, '-zvel', inds=inds,
                                                                x_conv_type=self.coords_conv_type,
                                                                y_conv_type=self.coords_conv_type, xlimit=self.xlim2d,
                                                                ylimit=self.ylim2d, nbx=self.nb, weight_type='mass',
                                                                quantity_conv_type=self.global_conv_type,
                                                                funcforpl=identity_function, vmin_plot=vmin,
                                                                vmax_plot=vmax, add_eps_in_plot=False,
                                                                fig=self.fig, cb_loc=self.ax2d)
        else:
            if (compstr == 'gas') & (self.sph_on):
                h2d, edy, edx = self.snap.sph_density_2d(compstr, self.xstr, self.ystr, inds=inds,
                                                         x_conv_type=self.coords_conv_type,
                                                         y_conv_type=self.coords_conv_type, xlimit=self.xlim2d,
                                                         ylimit=self.ylim2d, nbx=self.nb,
                                                         surfdens_conv_type=self.global_conv_type,
                                                         funcforpl=np.log10, vmin_plot=vmin,
                                                         vmax_plot=vmax, add_eps_in_plot=self.add_eps_in_plot,
                                                         fig=self.fig, cb_loc=self.ax2d)
            else:
                h2d, edy, edx = self.snap.surface_density_2d(compstr, self.xstr, self.ystr, inds=inds,
                                                             x_conv_type=self.coords_conv_type,
                                                             y_conv_type=self.coords_conv_type, xlimit=self.xlim2d,
                                                             ylimit=self.ylim2d, nbx=self.nb,
                                                             surfdens_conv_type=self.global_conv_type,
                                                             cic_hist=self.cic_hists, funcforpl=np.log10,
                                                             vmin_plot=vmin, vmax_plot=vmax,
                                                             add_eps_in_plot=self.add_eps_in_plot, fig=self.fig,
                                                             cb_loc=self.ax2d)

            surf = (edy[1] - edy[0]) * (edx[1] - edx[0])
            mass_in_hist = np.sum(h2d) * surf
            mass_in_hist *= \
                BaseUnitsDefs.get_conversion_factor_from_string('mass', self.snap.dicunits, conv_type='astro') / \
                BaseUnitsDefs.get_conversion_factor_from_string('surfdens', self.snap.dicunits, conv_type='astro')

            dic_of_masses = self.snap.get_dic_of_components_masses(conv_type='astro')
            str_mass_comp_in_msun = compstr + ' in m sun'
            mtot_comp_in_snap = dic_of_masses[str_mass_comp_in_msun]
            frac_in_hist = np.around(mass_in_hist / mtot_comp_in_snap * 100, 3)
            strmasses = ' ' + format(mass_in_hist, '.2e') + r' [M$_{\odot}$], ' + str(frac_in_hist) + '%'
            strtit += strmasses
        plt.sca(self.ax2d)
        strtit += '  snap #' + str(self.snap.get_isnap())
        plt.title(strtit)
        self.ax2d.set_xlabel('Projected ' + self.ax2d.get_xlabel())
        self.ax2d.set_ylabel('Projected ' + self.ax2d.get_ylabel())
        if self.retrieve_current_2d_hist:
            self.current_2d_hist = h2d
            self.current_2d_yedges = edy
            self.current_2d_xedges = edx

    def calc_com_pv_of_particles_inside_2d_plot(self, list_comps):
        nshowncomps = len(list_comps)
        compos = np.zeros((nshowncomps, 3))
        comvel = np.zeros((nshowncomps, 3))
        masscomp = np.zeros((nshowncomps))
        indsbooltot = (self.snap.data['pos'][:, 0] * self.fac_conv_pos < self.xlim2d[1]) & (
                self.snap.data['pos'][:, 0] * self.fac_conv_pos > self.xlim2d[0]) & (
                              self.snap.data['pos'][:, 1] * self.fac_conv_pos < self.ylim2d[1]) & (
                              self.snap.data['pos'][:, 1] * self.fac_conv_pos > self.ylim2d[0])

        icomp = 0
        for compstr in list_comps:
            indscomp = self.snap.get_indexes_of_component(compstr)
            indsboolscomp = np.zeros((np.sum(self.snap.data['npart'])), dtype=bool)
            indsboolscomp[indscomp] = True
            inds = np.where(indsbooltot & indsboolscomp)[0]
            compos[icomp] = self.snap.get_com(inds)
            comvel[icomp] = self.snap.get_vel_of_com(inds)
            masscomp[icomp] = np.sum(self.snap.data['mass'][inds])
            icomp += 1

        compostot = np.zeros((3))
        comveltot = np.zeros((3))
        for i in range(0, nshowncomps):
            compostot += masscomp[i] * compos[i]
            comveltot += masscomp[i] * comvel[i]
        compostot /= np.sum(masscomp)
        comveltot /= np.sum(masscomp)
        self.snap.ctre_pos = compostot
        self.snap.ctre_vel = comveltot

    def plot_surf_dens_of_rad(self, xlimit=None, ylimit_densofr=None, ylimit_velofr=None, ylimit_dispvofr=None):
        """Plots surface density/volume density/mass-weighted histogram of particles, cylindrical/spherical/los velocity
        and cylindrical/spherical/los velocity dispersion as a function of cylindrical radius/spherical radius/x axis for
        self.plotted_1D = 'cyl'/'spher'/'PV'.
        PV diagrams use the x_projected axis as x axis.

        Parameters
        ----------
        xlimit : (float, float) or None, optional.
            x limits for the 3 plots. (Default value = None)
        ylimit_densofr : (float, float) or None, optional.
            y limits for the density plot. (Default value = None)
        ylimit_velofr : (float, float) or None, optional.
            y limits for the velocity plot. (Default value = None)
        ylimit_dispvofr : (float, float) or None, optional.
            y limits for the velocity dispersion plot. (Default value = None)
        """

        plt.sca(self.axdensofr)
        if (xlimit is None):
            xlimit = self.axdensofr.get_xlim()
        if (ylimit_densofr is None):
            ylimit_densofr = self.axdensofr.get_ylim()
        if (ylimit_velofr is None):
            ylimit_velofr = self.axvelofr.get_ylim()
        if (ylimit_dispvofr is None):
            ylimit_dispvofr = self.axdispvofr.get_ylim()
        if ((xlimit == (0, 1)) & (ylimit_densofr == (0, 1))):  # new plot
            xlimit = ylimit_densofr = ylimit_velofr = ylimit_dispvofr = None
        if self.new_1d_geom:
            ylimit_densofr = ylimit_velofr = ylimit_dispvofr = None

        self.axdensofr.cla()
        listls = ['-', '--', '-.']
        if self.plotted_1d == 'cyl':
            xedges = linearly_spaced_edges_in_function_from_bin_size((0, 40.), identity_function, 1.)
            listvels = ['v_R', 'v_phi', 'zvel']
        elif self.plotted_1d == 'spher':
            xedges = linearly_spaced_edges_in_function_from_bin_size((1e-2, 1e2), np.log10, 0.05)
            listvels = ['velradspher', 'velthetaspher', 'v_phi']
        else:
            xedges = linearly_spaced_edges_in_function_from_bin_size((-40, 40.), identity_function, 1.)
            listvels = []
        for compstr in self.list_comps:
            col = self.dic_cols[compstr]
            if self.plotted_1d == 'cyl':
                self.snap.surface_density_of_radius(compstr, edges=xedges, fill_range=True, show_plot=True,
                                                    funcofyforpl=np.log10, xlimit_pl=xlimit, ylimit_pl=ylimit_densofr,
                                                    col=col)
            elif self.plotted_1d == 'spher':
                self.snap.volume_density_of_radius(compstr, funcofx=np.log10, edges=xedges, fill_range=True,
                                                   show_plot=True, funcofyforpl=np.log10, xlimit_pl=xlimit,
                                                   ylimit_pl=ylimit_densofr, col=col)
            else:
                self.snap.weighted_hist_of_array(compstr, 'xpos', funcofyforpl=identity_function, show_plot=True,
                                                 xlimit_pl=xlimit, col=col, edges=xedges, ylimit_pl=ylimit_densofr,
                                                 fill_range=True, weight_type='mass')
                self.axdensofr.set_ylabel('Mass at x')
        plt.sca(self.axvelofr)
        self.axvelofr.cla()
        for compstr in self.list_comps:
            col = self.dic_cols[compstr]
            if self.plotted_1d == 'cyl':
                [self.snap.binned_quantity_in_1d(compstr, 'radcyl', velstr, edges=xedges, fill_range=True,
                                                 weight_type='mass', show_plot=True, funcofyforpl=identity_function,
                                                 xlimit_pl=xlimit, ylimit_pl=ylimit_velofr, linestyle=ls, col=col)
                 for velstr, ls in zip(listvels, listls)]
            elif self.plotted_1d == 'spher':
                [self.snap.binned_quantity_in_1d(compstr, 'radspher', velstr, funcofx=np.log10, edges=xedges,
                                                 fill_range=True, weight_type='mass', show_plot=True,
                                                 funcofyforpl=identity_function, xlimit_pl=xlimit,
                                                 ylimit_pl=ylimit_velofr, linestyle=ls, col=col)
                 for velstr, ls in zip(listvels, listls)]
            else:
                self.snap.binned_quantity_in_1d(compstr, 'xpos', '-zvel', edges=xedges, fill_range=True,
                                                weight_type='mass', show_plot=True, funcofyforpl=identity_function,
                                                xlimit_pl=xlimit, ylimit_pl=ylimit_velofr, col=col)

        plt.sca(self.axdispvofr)
        self.axdispvofr.cla()
        for compstr in self.list_comps:
            col = self.dic_cols[compstr]
            if self.plotted_1d == 'cyl':
                [self.snap.velocity_dispersion_of_radius(compstr, xstr='radcyl', ystr=velstr, edges=xedges,
                                                         fill_range=True, show_plot=True, funcofyforpl=np.log10,
                                                         xlimit_pl=xlimit, ylimit_pl=ylimit_dispvofr, linestyle=ls,
                                                         col=col)
                 for velstr, ls in zip(listvels, listls)]
                self.axdispvofr.text(0.05, 0.05, r'$-$ v$_R$  $--$ v$_{\phi}$  -. v$_z$',
                                     transform=self.axdispvofr.transAxes)
            elif self.plotted_1d == 'spher':
                [self.snap.velocity_dispersion_of_radius(compstr, xstr='radspher', ystr=velstr, funcofx=np.log10,
                                                         edges=xedges, fill_range=True, show_plot=True,
                                                         funcofyforpl=np.log10, xlimit_pl=xlimit,
                                                         ylimit_pl=ylimit_dispvofr, linestyle=ls, col=col) for
                 velstr, ls in zip(listvels, listls)]
                self.axdispvofr.text(0.05, 0.05, r'$-$ v$_r$  $--$ v$_{\theta}$  -. v$_{\phi}$',
                                     transform=self.axdispvofr.transAxes)
            else:
                self.snap.velocity_dispersion_of_radius(compstr, xstr='xpos', ystr='-zvel', edges=xedges,
                                                        fill_range=True, show_plot=True, funcofyforpl=np.log10,
                                                        xlimit_pl=xlimit, ylimit_pl=ylimit_dispvofr, col=col)
        plt.setp(self.axdensofr.get_xticklabels(), visible=False)
        plt.setp(self.axvelofr.get_xticklabels(), visible=False)
        self.axdensofr.set_xlabel(' ')
        self.axvelofr.set_xlabel(' ')
        self.new_1d_geom = False

    def plot_quiv_2d_vels(self, compstr):
        """Plots quiver plot of velocities projected on plane in sparsed bins, to avoid crowdedeness (1 in 8 bins in x,
        and same in y).

        Parameters
        ----------
        compstr : str
            Member of self.snap.listcomps.
        """
        plt.sca(self.ax2d)
        if ((self.plot_recent_sf) & (self.compstr_hist == 'new stars')):
            tsnapingyr = self.snap.data['time'] * BaseUnitsDefs.get_conversion_factor_from_string('time',
                                                                                                  self.snap.dicunits,
                                                                                                  conv_type='astro')
            inds = self.snap.select_new_stars_indexes_with_tbirth((tsnapingyr - 0.1, tsnapingyr),
                                                                  conv_type='None')  # for SF < 100 Myr
        else:
            inds = None
        self.snap.quiver(compstr, self.xstr, self.ystr, 'xvel', 'yvel', xlimit=self.xlim2d, ylimit=self.ylim2d,
                         nbx=self.nb // 8, remove_average=self.substract_av_value, inds=inds, ids=self.ids,
                         x_conv_type=self.coords_conv_type, y_conv_type=self.coords_conv_type,
                         quantity_conv_type=self.global_conv_type)

    def plot_u_rho(self):
        if (self.xlimurho is None):
            self.xlimurho = (-12, 4)
            self.ylimurho = (9.5, 15.5)
            self.ylimurho = (1, 8)

        xlimurhoforpl = (10 ** self.xlimurho[0], 10 ** self.xlimurho[1])
        ylimurhoforpl = (10 ** self.ylimurho[0], 10 ** self.ylimurho[1])
        plt.sca(self.axurho)
        self.axurho.cla()
        if (self.axurhoxz is not None):
            self.axurhoxz.remove()
            self.axurhozy.remove()

        hm2d, hmrho, hmu, edy, edx, axy, axz, azy = \
            self.snap.weighted_hist_2d_with_marginals('gas', 'nH', 'temperature', x_conv_type='cgs', y_conv_type=None,
                                                      xlimit=xlimurhoforpl, ylimit=ylimurhoforpl, nbx=128, nby=128,
                                                      fig=self.fig, ax=self.axurho)

        self.axurhoxz = axz
        self.axurhozy = azy
        plt.sca(self.axurho)
        plt.draw()
        plt.show()

    def create_buttons(self):
        """Creates buttons using GalViewer.ButtonSetView.
        Returns
        -------
        :class:`~galaximview.galviewer.GalViewer.ButtonsActions` object.
            GalViewer.ButtonsActions object.
        """
        y_bot = 0.005
        y_top = 0.995
        x_right = 0.995
        x_left = 0.01
        button_height = 0.05
        small_button_height = button_height / 2.
        button_width = 0.05
        small_button_width = button_width / 2.
        vlarge_width = 0.06
        inter_width = 0.04
        dic_2dview = {
            'zoom ini': self.fig.add_axes([0.57, y_top - small_button_height, inter_width, small_button_height]),
            'zoom 3d': self.fig.add_axes(
                [0.57 + inter_width, y_top - small_button_height, inter_width, small_button_height]),
            'set view': self.fig.add_axes([0.45, y_top - button_height, button_width, button_height]),
            'nbins ini': self.fig.add_axes([0.51, y_top - small_button_height, inter_width, small_button_height]),
            'nbins*2': self.fig.add_axes([0.51, y_top - button_height, 0.02, small_button_height]),
            'nbins/2': self.fig.add_axes([0.53, y_top - button_height, 0.02, small_button_height]),
            'set cbar': self.fig.add_axes(
                [x_right - inter_width, y_top - 1.2 * button_height, inter_width, 1.2 * button_height])}
        dic_timecontrol = {
            'step min': self.fig.add_axes([0.68, y_top - small_button_height, inter_width, small_button_height]),
            'step*2': self.fig.add_axes([0.68, y_top - button_height, inter_width / 2., small_button_height]),
            'step/2': self.fig.add_axes(
                [0.68 + inter_width / 2., y_top - button_height, inter_width / 2., small_button_height]),
            'tbeg': self.fig.add_axes([0.73, y_top - small_button_height, small_button_width, small_button_height]),
            'previous': self.fig.add_axes(
                [0.73 + small_button_width, y_top - small_button_height, small_button_width, small_button_height]),
            'next': self.fig.add_axes(
                [0.73 + 2 * small_button_width, y_top - small_button_height, small_button_width, small_button_height]),
            'tend': self.fig.add_axes(
                [0.73 + 3 * small_button_width, y_top - small_button_height, small_button_width, small_button_height]),
            'play': self.fig.add_axes(
                [0.73 + 4 * small_button_width, y_top - small_button_height, small_button_width, small_button_height]),
            'pause': self.fig.add_axes(
                [0.73 + 5 * small_button_width, y_top - small_button_height, small_button_width, small_button_height])}
        dic_onoff3d = {'gas': self.fig.add_axes([x_left + inter_width, y_bot, inter_width, button_height]),
                       'all stars': self.fig.add_axes([x_left + 2 * inter_width, y_bot, inter_width, button_height]),
                       'disc stars': self.fig.add_axes(
                           [x_left + 3 * inter_width, y_bot + small_button_height, inter_width, small_button_height]),
                       'bulge stars': self.fig.add_axes(
                           [x_left + 4 * inter_width, y_bot + small_button_height, inter_width, small_button_height]),
                       'old stars': self.fig.add_axes(
                           [x_left + 3 * inter_width, y_bot, 2 * inter_width, small_button_height]),
                       'new stars': self.fig.add_axes([x_left + 5 * inter_width, y_bot, inter_width, button_height]),
                       'DM': self.fig.add_axes([x_left + 6 * inter_width, y_bot, inter_width, button_height])}
        dic_show2d = {'gas': self.fig.add_axes([0.575 - inter_width, y_bot, inter_width, button_height]),
                      'all stars': self.fig.add_axes([0.575, y_bot, inter_width, button_height]),
                      'disc stars': self.fig.add_axes(
                          [0.575 + inter_width, y_bot + small_button_height, inter_width, small_button_height]),
                      'bulge stars': self.fig.add_axes(
                          [0.575 + 2 * inter_width, y_bot + small_button_height, inter_width, small_button_height]),
                      'old stars': self.fig.add_axes(
                          [0.575 + inter_width, y_bot, 2 * inter_width, small_button_height]),
                      'new stars': self.fig.add_axes([0.575 + 3 * inter_width, y_bot, inter_width, button_height]),
                      'DM': self.fig.add_axes(
                          [0.575 + 3 * inter_width + inter_width, y_bot, inter_width, button_height])}
        dic_2dkin = {'los vel': self.fig.add_axes([0.79, y_bot, inter_width, button_height]),
                     'v2D': self.fig.add_axes([0.84, y_bot + small_button_height, inter_width, small_button_height]),
                     '-<v>': self.fig.add_axes([0.84, y_bot, inter_width, small_button_height]),
                     'sigma v_los': self.fig.add_axes([0.89, y_bot, inter_width, button_height])}
        dic_3dview = {
            'edge on': self.fig.add_axes([0.085, y_top - small_button_height, inter_width, small_button_height]),
            'face on': self.fig.add_axes([0.085, y_top - button_height, inter_width, small_button_height]),
            'elev ini': self.fig.add_axes(
                [0.13, y_top - small_button_height, 0.38 * 1.1 * vlarge_width, small_button_height]),
            'save elev': self.fig.add_axes([0.13, y_top - button_height, small_button_width, small_button_height]),
            'set elev': self.fig.add_axes(
                [0.13 + 1.1 * vlarge_width - small_button_width, y_top - button_height, small_button_width,
                 small_button_height]),
            'elev + 90': self.fig.add_axes(
                [0.13 + 0.38 * 1.1 * vlarge_width, y_top - small_button_height, 0.31 * 1.1 * vlarge_width,
                 small_button_height]),
            'elev - 90': self.fig.add_axes(
                [0.13 + 0.69 * 1.1 * vlarge_width, y_top - small_button_height, 0.31 * 1.1 * vlarge_width,
                 small_button_height]),
            'azim ini': self.fig.add_axes(
                [0.2, y_top - small_button_height, 0.38 * 1.1 * vlarge_width, small_button_height]),
            'save azim': self.fig.add_axes(
                [0.2, y_top - button_height, small_button_width, small_button_height]),
            'set azim': self.fig.add_axes(
                [0.2 + 1.1 * vlarge_width - small_button_width, y_top - button_height, small_button_width,
                 small_button_height]),
            'azim + 90': self.fig.add_axes(
                [0.2 + 0.38 * 1.1 * vlarge_width, y_top - small_button_height, 0.31 * 1.1 * vlarge_width,
                 small_button_height]),
            'azim - 90': self.fig.add_axes(
                [0.2 + 0.69 * 1.1 * vlarge_width, y_top - small_button_height, 0.31 * 1.1 * vlarge_width,
                 small_button_height]),
            'Zoom ini 3D': self.fig.add_axes([0.2725, y_top - small_button_height, button_width, small_button_height]),
            '2D to 3D': self.fig.add_axes([0.2725, y_top - button_height, button_width, small_button_height]),
            'frac ini': self.fig.add_axes(
                [0.33, y_top - small_button_height, 2. / 3. * button_width, small_button_height]),
            'frac*2': self.fig.add_axes([0.33, y_top - button_height, small_button_width, small_button_height]),
            'frac/2': self.fig.add_axes([0.355, y_top - button_height, small_button_width, small_button_height]),
            'just 3D': self.fig.add_axes([0.3, y_bot, small_button_width, button_height])}
        dic_1d = {'1d': self.fig.add_axes(
            [0.31 + small_button_width, y_bot + small_button_height, vlarge_width, small_button_height]),
            'PV': self.fig.add_axes([0.31 + small_button_width, y_bot, vlarge_width / 3., small_button_height]),
            'Cyl': self.fig.add_axes([0.31 + small_button_width + vlarge_width / 3., y_bot, vlarge_width / 3.,
                                      small_button_height]),
            'Spher': self.fig.add_axes([0.31 + small_button_width + 2 * vlarge_width / 3., y_bot, vlarge_width / 3.,
                                        small_button_height])}
        dic_gas = {'u vs nH': self.fig.add_axes([0.4, y_bot, small_button_width, small_button_height]),
                   'SPH': self.fig.add_axes([0.55, y_top - button_height, 0.5 * small_button_width, button_height])}
        dic_new_buttons = {'New button': self.fig.add_axes([0.45, y_bot, inter_width, small_button_height])}

        for dic in [dic_onoff3d, dic_3dview, dic_show2d, dic_2dview, dic_2dkin, dic_timecontrol, dic_1d,
                    dic_new_buttons]:
            for key in dic.keys():
                dic[key].set_label(key)

        return GalViewer.ButtonsActions(self, dic_onoff3d, dic_3dview, dic_show2d, dic_2dview, dic_2dkin,
                                        dic_timecontrol, dic_1d, dic_gas, dic_new_buttons)

    class ButtonsActions(object):
        """Subclass to create buttons.

        Parameters
        ----------
        outer : GalViewer object
            Outer instance, i.e. GalViewer object.
        dic_onoff3d : dictionary
            Dictionary of axes of buttons for 3D view of components.
        dic_3dview : dictionary
            Dictionary of axes of buttons for 3D view.
        dic_show2d : dictionary
            Dictionary of axes of buttons for 2D view of components.
        dic_2dview : dictionary
            Dictionary of axes of buttons for 2D view.
        dic_2dkin : dictionary
            Dictionary of axes of buttons for 2D kinematics view.
        dic_timecontrol : dictionary
            Dictionary of axes of buttons for navigation in simulation.
        dic_1d : dictionary
            Dictionary of axes of buttons for 1D plots.
        """

        def __init__(self, outer, dic_onoff3d, dic_3dview, dic_show2d, dic_2dview, dic_2dkin, dic_timecontrol, dic_1d,
                     dic_gas, dic_new_buttons):
            self.outer_instance = outer
            self.icurr = self.outer_instance.ibeg
            self.on_play = True
            dic_labsfuncs2dview = {'zoom ini': ['Zoom ini', self.action_zoom_ini],
                                   'zoom 3d': ['Zoom 3D', self.action_zoom_3d],
                                   'set view': ['Set view', self.action_set_view],
                                   'nbins ini': ['Nb ini', self.action_nbini], 'nbins*2': ['x2', self.action_nbt2],
                                   'nbins/2': ['/2', self.action_nbon2],
                                   'set cbar': ['Keep\nlimits of\ncol. bar', self.action_cbar]}
            self.dic_buts_2dview = {}
            dic_labsfuncstimecontrol = {'step min': ['Step ini', self.action_step],
                                        'step*2': ['x2', self.action_stept2],
                                        'step/2': ['/2', self.action_stepon2], 'tbeg': ['|<<', self.to_tbeg],
                                        'previous': ['<<', self.prev], 'next': ['>>', self.next],
                                        'tend': ['>>|', self.to_tend], 'play': [r'$\blacktriangleright$', self.play],
                                        'pause': ['||', self.action_pause]}
            self.dic_buts_timecontrol = {}
            dic_labsfuncsonoff3d = {'gas': ['Gas', self.add_rem_gas], 'all stars': ['All *', self.add_rem_all_stars],
                                    'disc stars': ['Disc *', self.add_rem_disc],
                                    'bulge stars': ['Bulge *', self.add_rem_bulge],
                                    'old stars': ['Old *', self.add_rem_old_stars],
                                    'new stars': ['New *', self.add_rem_new_stars],
                                    'DM': ['DM', self.add_rem_dark_matter]}
            self.dic_buts_onoff3d = {}
            dic_labsfuncsshow2d = {'gas': ['Gas', self.hist_gas], 'all stars': ['All *', self.hist_all_stars],
                                   'disc stars': ['Disc *', self.hist_disc],
                                   'bulge stars': ['Bulge *', self.hist_bulge],
                                   'old stars': ['Old *', self.hist_old_stars],
                                   'new stars': ['New *', self.hist_new_stars], 'DM': ['DM', self.hist_dark_matter]}
            self.dic_buts_show2d = {}
            dic_labsfuncs2dkin = {'los vel': [r'$\Sigma$ / v$_{\mathrm{los}}$', self.action_los_vel],
                                  'v2D': [r'v$_{\mathrm{2D}}$', self.action_quiv], '-<v>': [r'- <v>', self.action_rmav],
                                  'sigma v_los': [r'$\sigma_{\mathrm{los}}$', self.action_dispvel]}
            self.dic_buts_2dkin = {}
            dic_labsfuncs3dview = {'edge on': ['Edge-on', self.action_edge_on],
                                   'face on': ['Face-on', self.action_face_on],
                                   'elev ini': ['elev i', self.action_elevini],
                                   'save elev': ['Save', self.action_save_elev],
                                   'set elev': ['Set', self.action_set_elev],
                                   'elev + 90': [r'+$\pi$/2', self.action_elevplus90],
                                   'elev - 90': [r'-$\pi$/2', self.action_elevminus90],
                                   'azim ini': ['azim i', self.action_azim_0],
                                   'save azim': ['Save', self.action_save_azim],
                                   'set azim': ['Set', self.action_set_azim],
                                   'azim + 90': [r'+$\pi$/2', self.action_azimplus90],
                                   'azim - 90': [r'-$\pi$/2', self.action_azimminus90],
                                   'Zoom ini 3D': [r'(0,0,0) ini', self.action_zoom_ini_3d],
                                   '2D to 3D': ['2D->3D', self.action_2dto3d],
                                   'frac ini': ['frac ini', self.action_fracini], 'frac*2': ['x2', self.action_fract2],
                                   'frac/2': ['/2', self.action_fracon2], 'just 3D': ['3D \nonly', self.action_plot_3d]}
            self.dic_buts_3dview = {}
            dic_labsfuncs1d = {'1d': ['1D', self.action_plot_fr], 'Cyl': ['R', self.action_cyl],
                               'Spher': ['r', self.action_spher], 'PV': ['PV', self.action_pv]}
            self.dic_buts_1d = {}

            dic_labsfuncsgas = {'u vs nH': [r'T $\rho$', self.action_plot_u_rho],
                                'SPH': ['S\nP\nH', self.action_sph]}
            self.dic_buts_gas = {}

            dic_labsfuncsnewbuttons = {'New button': ['Nothing', self.action_nothing]}
            self.dic_buts_newbuttons = {}  # this should be kept as such, dictonary is filled later

            list_dics_axes = [dic_onoff3d, dic_3dview, dic_show2d, dic_2dview, dic_2dkin, dic_timecontrol, dic_1d,
                              dic_gas, dic_new_buttons]
            list_dics_labsfuncs = [dic_labsfuncsonoff3d, dic_labsfuncs3dview, dic_labsfuncsshow2d, dic_labsfuncs2dview,
                                   dic_labsfuncs2dkin, dic_labsfuncstimecontrol, dic_labsfuncs1d,
                                   dic_labsfuncsgas, dic_labsfuncsnewbuttons]
            list_dics_buts = [self.dic_buts_onoff3d, self.dic_buts_3dview, self.dic_buts_show2d, self.dic_buts_2dview,
                              self.dic_buts_2dkin, self.dic_buts_timecontrol, self.dic_buts_1d,
                              self.dic_buts_gas, self.dic_buts_newbuttons]
            for dic_axes, dic_labsfuncs, dic_buts in zip(list_dics_axes, list_dics_labsfuncs, list_dics_buts):
                for string in dic_axes.keys():
                    dic_buts[string] = Button(dic_axes[string], dic_labsfuncs[string][0])
                    dic_buts[string].on_clicked(dic_labsfuncs[string][1])

            col = self.outer_instance.dic_cols[self.outer_instance.compstr]
            self.dic_buts_onoff3d[self.outer_instance.compstr].color, self.dic_buts_onoff3d[
                self.outer_instance.compstr].hovercolor = self.but_cols(col)
            self.dic_buts_show2d[self.outer_instance.compstr].color, self.dic_buts_show2d[
                self.outer_instance.compstr].hovercolor = self.but_cols(col)

            self.listbutsstategas2d = []
            self.listbutsactionsgas = [self.dic_buts_show2d['gas'], self.dic_buts_onoff3d['gas'],
                                       self.dic_buts_onoff3d['new stars'], self.dic_buts_show2d['new stars'],
                                       self.dic_buts_gas['u vs nH']]
            self.listbutscinem = list(self.dic_buts_2dkin.values())

            self.shade_buttons_absent_comps()

        def action_nothing(self, event):
            """Example, does nothing."""
            self._treat_event(event)

        def shade_buttons_absent_comps(self):
            if 'gas' not in self.outer_instance.snap.listcomps:
                for button in self.listbutsactionsgas:
                    button.color, button.hovercolor = self.but_cols('k', alphamax=0.6, alphamin=0.6)

            for compstr in ['gas', 'DM', 'disc stars', 'bulge stars', 'new stars', 'old stars', 'all stars']:
                if compstr not in self.outer_instance.snap.listcomps:
                    button = self.dic_buts_onoff3d[compstr]
                    button.color, button.hovercolor = self.but_cols('k', alphamax=0.6, alphamin=0.6)
                    button = self.dic_buts_show2d[compstr]
                    button.color, button.hovercolor = self.but_cols('k', alphamax=0.6, alphamin=0.6)
                else:
                    button = self.dic_buts_onoff3d[compstr]
                    button.color, button.hovercolor = self.but_cols('k', alphamax=0.15, alphamin=0.05)
                    col = self.outer_instance.dic_cols[compstr]
                    if compstr in self.outer_instance.list_comps:
                        button.color, button.hovercolor = self.but_cols(col)
                    button = self.dic_buts_show2d[compstr]
                    button.color, button.hovercolor = self.but_cols('k', alphamax=0.15, alphamin=0.05)
                    if compstr == self.outer_instance.compstr_hist:
                        button.color, button.hovercolor = self.but_cols(col)

        def _treat_event(self, event):
            """ """
            if event.inaxes is not None:
                event.inaxes.figure.canvas.draw_idle()

        def action_fracini(self, event):
            """  """
            self.outer_instance.frac = self.outer_instance.frac_ini
            self.outer_instance.plot_comps_in_3d()
            self.outer_instance.write_frac()
            self._treat_event(event)

        def action_fract2(self, event):
            """ """
            self.outer_instance.frac *= 2
            if self.outer_instance.frac > 1:
                self.outer_instance.frac = 1
            self.outer_instance.plot_comps_in_3d()
            self.outer_instance.write_frac()
            self._treat_event(event)

        def action_fracon2(self, event):
            """  """
            self.outer_instance.frac /= 2
            self.outer_instance.plot_comps_in_3d()
            self.outer_instance.write_frac()
            self._treat_event(event)

        def action_edge_on(self, event):
            """ """
            self.outer_instance.ax3d.elev = 0
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_face_on(self, event):
            """  """
            self.outer_instance.ax3d.elev = 90
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_elevini(self, event):
            """  """
            self.outer_instance.ax3d.elev = 30
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_save_elev(self, event):
            """  """
            self.outer_instance.saved_elev = self.outer_instance.ax3d.elev
            self.dic_buts_3dview['save elev'].color, self.dic_buts_3dview['save elev'].hovercolor = \
                self.but_cols('C6', alphamax=1., alphamin=0.75)
            self._treat_event(event)

        def action_set_elev(self, event):
            """ """
            self.outer_instance.ax3d.elev = self.outer_instance.saved_elev
            self.dic_buts_3dview['save elev'].color, self.dic_buts_3dview['save elev'].hovercolor = \
                self.but_cols('k', alphamax=0.15, alphamin=0.05)
            self._treat_event(event)

        def action_elevplus90(self, event):
            """    """
            self.outer_instance.ax3d.elev += 90
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_elevminus90(self, event):
            """  """
            self.outer_instance.ax3d.elev -= 90
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_azim_0(self, event):
            """  """
            self.outer_instance.ax3d.azim = -90
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_save_azim(self, event):
            """   """
            self.outer_instance.saved_azim = self.outer_instance.ax3d.azim
            self.dic_buts_3dview['save azim'].color, self.dic_buts_3dview['save azim'].hovercolor = \
                self.but_cols('C6', alphamax=1., alphamin=0.75)
            self._treat_event(event)

        def action_set_azim(self, event):
            """    """
            self.outer_instance.ax3d.azim = self.outer_instance.saved_azim
            self.dic_buts_3dview['save azim'].color, self.dic_buts_3dview['save azim'].hovercolor = \
                self.but_cols('k', alphamax=0.15, alphamin=0.05)
            self._treat_event(event)

        def action_azimplus90(self, event):
            """   """
            self.outer_instance.ax3d.azim += 90
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_azimminus90(self, event):
            """    """
            self.outer_instance.ax3d.azim -= 90
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_2dto3d(self, event):
            """     """
            self.outer_instance.match_2d_view()
            self.outer_instance.match_3d_view()
            self._treat_event(event)

        def add_rem_gas(self, event):
            """  """
            string = 'gas'
            self.add_rem_comp(string, self.dic_buts_onoff3d[string], event)

        def add_rem_all_stars(self, event):
            """ """
            string = 'all stars'
            self.add_rem_comp(string, self.dic_buts_onoff3d[string], event)

        def add_rem_disc(self, event):
            """  """
            string = 'disc stars'
            self.add_rem_comp(string, self.dic_buts_onoff3d[string], event)

        def add_rem_bulge(self, event):
            """          """
            string = 'bulge stars'
            self.add_rem_comp(string, self.dic_buts_onoff3d[string], event)

        def add_rem_old_stars(self, event):
            """   """
            string = 'old stars'
            self.add_rem_comp(string, self.dic_buts_onoff3d[string], event)

        def add_rem_new_stars(self, event):
            """    """
            string = 'new stars'
            self.add_rem_comp(string, self.dic_buts_onoff3d[string], event)

        def add_rem_dark_matter(self, event):
            """    """
            string = 'DM'
            self.add_rem_comp(string, self.dic_buts_onoff3d[string], event)

        def add_rem_comp(self, compstr, but, event):
            """
            Parameters
            ----------
            compstr : str
                Member of self.outer_instance.snap.listcomps.
            but : matplotlib button
            """
            if compstr in self.outer_instance.snap.listcomps:
                if (compstr in self.outer_instance.list_comps):
                    self.outer_instance.list_comps.remove(compstr)
                    but.color, but.hovercolor = self.but_cols('k', alphamin=0.05, alphamax=0.15)
                else:
                    self.outer_instance.list_comps.append(compstr)
                    col = self.outer_instance.dic_cols[compstr]
                    but.color, but.hovercolor = self.but_cols(col)
                if self.outer_instance.ax3d is not None:
                    self._action_replot_3d(event)
                if self.outer_instance.axdensofr is not None:
                    self._action_replot_sd(event)

        def _action_replot_3d(self, event):
            """ """
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def _action_replot_sd(self, event):
            """ """
            self.outer_instance.plot_surf_dens_of_rad()
            self._treat_event(event)

        def _save_3d_params(self):
            """ """
            if self.outer_instance.ax3d is not None:
                self.outer_instance.azim = self.outer_instance.ax3d.azim
                self.outer_instance.elev = self.outer_instance.ax3d.elev
                self.outer_instance.xlimit_3d = self.outer_instance.ax3d.get_xlim()
                self.outer_instance.ylimit_3d = self.outer_instance.ax3d.get_ylim()
                self.outer_instance.zlimit_3d = self.outer_instance.ax3d.get_zlim()
                self.outer_instance.fig.delaxes(self.outer_instance.ax3d)
                self.outer_instance.ax3d = None

        def _save_sd_params(self):
            """ """
            if self.outer_instance.axdensofr is not None:
                self.outer_instance.xlimit_densofr = self.outer_instance.axdensofr.get_xlim()
                self.outer_instance.ylimit_densofr = self.outer_instance.axdensofr.get_ylim()
                self.outer_instance.fig.delaxes(self.outer_instance.axdensofr)
                self.outer_instance.axdensofr = None
                self.outer_instance.xlimit_velofr = self.outer_instance.axvelofr.get_xlim()
                self.outer_instance.ylimit_velofr = self.outer_instance.axvelofr.get_ylim()
                self.outer_instance.fig.delaxes(self.outer_instance.axvelofr)
                self.outer_instance.axvelofr = None
                self.outer_instance.xlimit_dispvofr = self.outer_instance.axdispvofr.get_xlim()
                self.outer_instance.ylimit_dispvofr = self.outer_instance.axdispvofr.get_ylim()
                self.outer_instance.fig.delaxes(self.outer_instance.axdispvofr)
                self.outer_instance.axdispvofr = None

        def _save_urho_params(self):
            if self.outer_instance.axurho is not None:
                self.outer_instance.fig.delaxes(self.outer_instance.axurho)
                self.outer_instance.fig.delaxes(self.outer_instance.axurhoxz)
                self.outer_instance.fig.delaxes(self.outer_instance.axurhozy)
                self.outer_instance.axurho = None
                self.outer_instance.axurhoxz = None
                self.outer_instance.axurhozy = None

        def action_plot_3d(self, event):
            """ """
            if self.outer_instance.just_3d:
                return
            self.outer_instance.just_3d = True
            self._save_sd_params()
            self._save_urho_params()
            self._save_3d_params()
            self.outer_instance.ax3d = self.outer_instance.fig.add_subplot(1, 2, 1, projection='3d')
            self.outer_instance.plot_comps_in_3d(xlimit=self.outer_instance.xlimit_3d,
                                                 ylimit=self.outer_instance.xlimit_3d,
                                                 zlimit=self.outer_instance.xlimit_3d, elev=self.outer_instance.elev,
                                                 azim=self.outer_instance.azim)
            self._treat_event(event)

        def action_plot_fr(self, event):
            """ """
            if self.outer_instance.axdensofr is not None:
                return
            self._save_urho_params()
            self.outer_instance.just_3d = False
            self._save_3d_params()
            self.outer_instance.ax3d = self.outer_instance.fig.add_subplot(self.outer_instance.figgs[:10, 0:4],
                                                                           projection='3d')
            self.outer_instance.plot_comps_in_3d(xlimit=self.outer_instance.xlimit_3d,
                                                 ylimit=self.outer_instance.xlimit_3d,
                                                 zlimit=self.outer_instance.xlimit_3d, elev=self.outer_instance.elev,
                                                 azim=self.outer_instance.azim)
            self.outer_instance.axdensofr = self.outer_instance.fig.add_subplot(self.outer_instance.figgs[0:5, 5:10])
            self.outer_instance.axvelofr = self.outer_instance.fig.add_subplot(self.outer_instance.figgs[5:10, 5:10],
                                                                               sharex=self.outer_instance.axdensofr)
            self.outer_instance.axdispvofr = self.outer_instance.fig.add_subplot(self.outer_instance.figgs[10:15, 5:10],
                                                                                 sharex=self.outer_instance.axdensofr)
            if self.outer_instance.plotted_1d == 'cyl':
                self.action_cyl(event)
            elif self.outer_instance.plotted_1d == 'spher':
                self.action_spher(event)
            else:
                self.action_pv(event)
            self._treat_event(event)

        def action_cyl(self, event):
            """ """
            if self.outer_instance.axdensofr is None:
                return
            self.outer_instance.plotted_1d = 'cyl'
            self.outer_instance.new_1d_geom = True
            sidex = (self.outer_instance.xlim2d[1] - self.outer_instance.xlim2d[0])
            self.outer_instance.plot_surf_dens_of_rad(xlimit=(-0.1, 0.5 * sidex))
            self._treat_event(event)

        def action_spher(self, event):
            """ """
            if self.outer_instance.axdensofr is None:
                return
            self.outer_instance.plotted_1d = 'spher'
            self.outer_instance.new_1d_geom = True
            sidex = (self.outer_instance.xlim2d[1] - self.outer_instance.xlim2d[0])
            self.outer_instance.plot_surf_dens_of_rad(xlimit=(-1, np.log10(np.fabs(0.5 * sidex))))
            self._treat_event(event)

        def action_pv(self, event):
            """ """
            if self.outer_instance.axdensofr is None:
                return
            self.outer_instance.plotted_1d = 'PV'
            self.outer_instance.new_1d_geom = True
            self.outer_instance.plot_surf_dens_of_rad(
                xlimit=(self.outer_instance.xlim2d[0], self.outer_instance.xlim2d[1]))
            self._treat_event(event)

        def action_set_view(self, event):
            """Replots 2D plot so that the projection corresponds to the viewgin angle of the 3D plots, the number of
            bins is the current number of bins.
            """
            self.outer_instance.match_3d_view()
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            if self.outer_instance.axdensofr is not None:
                self.outer_instance.plot_surf_dens_of_rad()
            self._treat_event(event)

        def action_time(self, event):
            """Performed when changing shown time by pressing one of the time-control buttons."""
            # set snapshot to the current one
            self.outer_instance.snap = self.outer_instance.simu.get_snap(self.icurr)
            # shade buttons of absent components
            self.shade_buttons_absent_comps()
            # get current limits of 2d plot
            self.outer_instance.xlim_2d = self.outer_instance.ax2d.get_xlim()
            self.outer_instance.ylim_2d = self.outer_instance.ax2d.get_ylim()
            # plot in 3D
            if self.outer_instance.ax3d is not None:
                self.outer_instance.plot_comps_in_3d()
            # plot in 1D if already on window
            if self.outer_instance.axdensofr is not None:
                self.outer_instance.plot_surf_dens_of_rad()
            # plot of u vs rho if already on window
            if self.outer_instance.axurho is not None:
                self.outer_instance.plot_u_rho()
            # plot in 2D
            self.outer_instance.match_3d_view()
            self.action_set_view(event)
            # show masses of components on top-left corner
            self.outer_instance.write_masses()

            self._treat_event(event)

        def play(self, event):
            """Plays simulations, i.e. shows successive snapshots from current one.

            Stopped by `~galaximview.galviewer.GalViewer.ButtonsActions.action_pause` """

            self.on_play = True
            while (self.icurr < self.outer_instance.simu.isnapmax):
                if self.on_play:
                    plt.pause(0.05)
                    self.icurr += self.outer_instance.step
                    self.action_time(event)
                    plt.pause(0.05)
                else:
                    break

        def action_pause(self, event):
            """Stops simulation from being played."""
            self.on_play = False
            self._treat_event(event)

        def action_step(self, event):
            """ """
            self.outer_instance.step = self.outer_instance.step_ini
            self.outer_instance.write_step()
            self._treat_event(event)

        def action_stept2(self, event):
            """ """
            self.outer_instance.step *= 2
            self.outer_instance.write_step()
            self._treat_event(event)

        def action_stepon2(self, event):
            """ """
            self.outer_instance.step //= 2
            self.outer_instance.write_step()
            self._treat_event(event)

        def next(self, event):
            """ """
            self.icurr += self.outer_instance.step
            if (self.icurr > self.outer_instance.simu.isnapmax):
                self.icurr = self.outer_instance.simu.isnapmax
            self.action_time(event)

        def prev(self, event):
            """ """
            self.icurr -= self.outer_instance.step
            if (self.icurr < self.outer_instance.ibeg):
                self.icurr = self.outer_instance.ibeg
            self.action_time(event)

        def to_tbeg(self, event):
            """ """
            self.icurr = self.outer_instance.ibeg
            self.action_time(event)

        def to_tend(self, event):
            """ """
            self.icurr = self.outer_instance.simu.isnapmax
            self.action_time(event)

        def action_zoom_ini(self, event):
            """ """
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist, zoom_ini=True)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_zoom_ini_3d(self, event):
            """ """
            self.outer_instance.vect_ctr_inv = np.zeros((3))
            self.outer_instance.plot_comps_in_3d()
            self._treat_event(event)

        def action_zoom_3d(self, event):
            """ """
            self.outer_instance.match_3d_view()
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist, zoom_3d=True)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_nbini(self, event):
            """ """
            self.outer_instance.nb = self.outer_instance.nb_ini
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_nbt2(self, event):
            """ """
            self.outer_instance.nb *= 2
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_nbon2(self, event):
            """ """
            self.outer_instance.nb /= 2
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_cbar(self, event):
            """ """
            if self.outer_instance.cbar_lims is None:
                plt.sca(self.outer_instance.ax2d)
                vmin = plt.gca().images[0].colorbar.vmin
                vmax = plt.gca().images[0].colorbar.vmax
                self.outer_instance.cbar_lims = (vmin, vmax)
                self.dic_buts_2dview['set cbar'].color, self.dic_buts_2dview['set cbar'].hovercolor = self.but_cols(
                    'C6', alphamax=1., alphamin=0.75)
            else:
                self.outer_instance.cbar_lims = None
                self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
                self.dic_buts_2dview['set cbar'].color, self.dic_buts_2dview['set cbar'].hovercolor = \
                    self.but_cols('k', alphamax=0.15, alphamin=0.05)
            self._treat_event(event)

        def action_los_vel(self, event):
            """ """
            self.dic_buts_2dkin['sigma v_los'].color, self.dic_buts_2dkin['sigma v_los'].hovercolor = self.but_cols(
                'k', alphamax=.15, alphamin=0.05)
            if self.outer_instance.plotted_2d == 'surf':
                self.outer_instance.plotted_2d = 'v los'
            else:
                self.outer_instance.plotted_2d = 'surf'
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_quiv(self, event):
            """ """
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
                self.outer_instance.quiver_plot = False
            else:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
                self.outer_instance.quiver_plot = True
            self._treat_event(event)

        def action_rmav(self, event):
            """  """
            if self.outer_instance.substract_av_value:
                self.outer_instance.substract_av_value = False
                self.dic_buts_2dkin['-<v>'].color, self.dic_buts_2dkin['-<v>'].hovercolor = self.but_cols('k',
                                                                                                          alphamax=0.15,
                                                                                                          alphamin=0.05)
            else:
                self.outer_instance.substract_av_value = True
                self.dic_buts_2dkin['-<v>'].color, self.dic_buts_2dkin['-<v>'].hovercolor = self.but_cols('C6',
                                                                                                          alphamax=1.,
                                                                                                          alphamin=0.75)
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_dispvel(self, event):
            """ """
            if self.outer_instance.plotted_2d == 'sigma v_los':
                self.outer_instance.plotted_2d = 'surf'
                self.dic_buts_2dkin['sigma v_los'].color, self.dic_buts_2dkin['sigma v_los'].hovercolor = self.but_cols(
                    'k', alphamax=.15, alphamin=0.05)
            else:
                self.outer_instance.plotted_2d = 'sigma v_los'
                self.dic_buts_2dkin['sigma v_los'].color, self.dic_buts_2dkin['sigma v_los'].hovercolor = self.but_cols(
                    'C6', alphamax=1., alphamin=0.75)
            self.outer_instance.plot_2d(self.outer_instance.compstr_hist)
            if self.outer_instance.quiver_plot:
                self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        @classmethod
        def but_cols(cls, col, alphamax=0.75, alphamin=0.5):
            """ """
            colrgba = mcolors.to_rgba(col)
            col = list(colrgba)
            colbut = col
            colbut[-1] = alphamax
            colbut = tuple(colbut)
            colhov = col
            colhov[-1] = alphamin
            colhov = tuple(colhov)
            return colbut, colhov

        def hist_gas(self, event):
            """ Executes :meth:`~galaximview.galviewer.GalViewer.ButtonSetView.action_hist_comp` for 'gas'
            when button 'Gas' is pressed."""
            self.action_hist_comp('gas', event)

        def hist_disc(self, event):
            """ Executes :meth:`~galaximview.galviewer.GalViewer.ButtonSetView.action_hist_comp` for 'disc stars'
            when button 'Disc ``*``' is pressed."""
            self.action_hist_comp('disc stars', event)

        def hist_bulge(self, event):
            """ Executes :meth:`~galaximview.galviewer.GalViewer.ButtonSetView.action_hist_comp` for 'bulge stars'
            when button 'Bulge ``*``' is pressed."""
            self.action_hist_comp('bulge stars', event)

        def hist_old_stars(self, event):
            """ Executes :meth:`~galaximview.galviewer.GalViewer.ButtonSetView.action_hist_comp` for 'old stars'
            when button 'Old ``*``' is pressed."""
            self.action_hist_comp('old stars', event)

        def hist_new_stars(self, event):
            """ Executes :meth:`~galaximview.galviewer.GalViewer.ButtonSetView.action_hist_comp` for 'new stars'
            when button 'New ``*``' is pressed."""
            self.action_hist_comp('new stars', event)

        def hist_all_stars(self, event):
            """ Executes :meth:`~galaximview.galviewer.GalViewer.ButtonSetView.action_hist_comp` for 'all stars'
            when button 'All ``*``' is pressed."""
            self.action_hist_comp('all stars', event)

        def hist_dark_matter(self, event):
            """ Executes :meth:`~galaximview.galviewer.GalViewer.ButtonSetView.action_hist_comp` for 'DM'
            when button 'DM' is pressed."""
            self.action_hist_comp('DM', event)

        def action_hist_comp(self, compstr, event):
            """

            Parameters
            ----------
            compstr : str
                Member of self.outer_instance.snap.listcomps.
            event :

            """
            for compstring in self.dic_buts_show2d.keys():
                if compstring in self.outer_instance.snap.listcomps:
                    self.dic_buts_show2d[compstring].color, self.dic_buts_show2d[compstring].hovercolor = \
                        self.but_cols('k', alphamin=0.05, alphamax=0.15)
            col = self.outer_instance.dic_cols[compstr]
            self.dic_buts_show2d[compstr].color, self.dic_buts_show2d[compstr].hovercolor = self.but_cols(col)
            if compstr in self.outer_instance.snap.listcomps:
                self.outer_instance.compstr_hist = compstr
                self.outer_instance.plot_2d(compstr)
                if self.outer_instance.quiver_plot:
                    self.outer_instance.plot_quiv_2d_vels(self.outer_instance.compstr_hist)
            self._treat_event(event)

        def action_plot_u_rho(self, event):
            if ((('rho' in self.outer_instance.snap.data.keys()) | (
                    'uint' in self.outer_instance.snap.data.keys())) is False):
                return
            if self.outer_instance.axurho is not None:
                return
            self._save_urho_params()
            self._save_sd_params()
            if self.outer_instance.just_3d:
                self.outer_instance.just_3d = False
                self._save_3d_params()
                self.outer_instance.ax3d = self.outer_instance.fig.add_subplot(self.outer_instance.figgs[:10, 0:4],
                                                                               projection='3d')
                self.outer_instance.plot_comps_in_3d(xlimit=self.outer_instance.xlimit_3d,
                                                     ylimit=self.outer_instance.xlimit_3d,
                                                     zlimit=self.outer_instance.xlimit_3d,
                                                     elev=self.outer_instance.elev,
                                                     azim=self.outer_instance.azim)
            self.outer_instance.axurho = self.outer_instance.fig.add_subplot(self.outer_instance.figgs[0:5, 5:10])
            self.outer_instance.axurhoxz = None
            self.outer_instance.axurhozy = None
            self.outer_instance.plot_u_rho()
            self._treat_event(event)

        def action_sph(self, event):
            if ((('rho' in self.outer_instance.snap.data.keys()) | (
                    'hsml' in self.outer_instance.snap.data.keys())) is False):
                return
            if self.outer_instance.sph_on:
                self.outer_instance.sph_on = False
                self.dic_buts_gas['SPH'].color, self.dic_buts_gas['SPH'].hovercolor = self.but_cols('k', alphamax=0.15,
                                                                                                    alphamin=0.05)
            else:
                self.outer_instance.sph_on = True
                self.dic_buts_gas['SPH'].color, self.dic_buts_gas['SPH'].hovercolor = self.but_cols('C6', alphamax=1.,
                                                                                                    alphamin=0.75)
            if self.outer_instance.compstr_hist == 'gas':  # replot 2D only if gas currently plotted
                self.action_hist_comp('gas', event)
